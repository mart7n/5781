#include <assert.h>
#include "SIDs.h"
#include "LoggedOnSID.h"

using namespace NTSecurity;

PSID NTSecurity::GetLoggedOnSID()
{
	static CLoggedOnSID sid;
	return sid.GetSID();
}

//////////////////////////////////////////////////////////////////////////////
// CLoggedOnSID members.
//////////////////////////////////////////////////////////////////////////////
CLoggedOnSID::CLoggedOnSID()
{
	DWORD dwResult = SIDs::GetLogOnAccountSID(m_pSID);
	assert(dwResult == ERROR_SUCCESS);
}

CLoggedOnSID::~CLoggedOnSID()
{
	if (m_pSID != NULL)
		::HeapFree(::GetProcessHeap(), 0, (LPVOID)m_pSID);
}

PSID CLoggedOnSID::GetSID()
{	
	return m_pSID;
}
