#pragma once

#include <windows.h>
#include <aclapi.h>

namespace IAMC
{
	namespace Security
	{
		class WinUtils
		{
		public:
			static bool IsWinNT();
		};

		//////////////////////////////////////////////////////////////////////
		// Class to wrap up the use of a public security descriptor which 
		// contains the EVERYONE group as read only and adminstrators as full 
		// access.  This can then be used when creating registry keys for 
		// example.  
		//////////////////////////////////////////////////////////////////////
		class PublicSecurityDescriptor
		{
		public:
			PublicSecurityDescriptor();
			~PublicSecurityDescriptor();

			DWORD CreatePublicSD();
			DWORD CreateOpenSD();
			PSECURITY_DESCRIPTOR GetPSD() const;
			PSECURITY_ATTRIBUTES GetPSA() const;

		private:
			void Cleanup();

			PSID                    m_pEveryoneSID;
			PSID                    m_pAdminSID;
			PACL                    m_pACL;
			PSECURITY_DESCRIPTOR    m_pSD;
			SECURITY_ATTRIBUTES     m_sa;
			PSECURITY_ATTRIBUTES    m_pSA;
		};

		//////////////////////////////////////////////////////////////////////
		// Automatically obtain the public security descriptor in the 
		// constructor.
		//////////////////////////////////////////////////////////////////////
		class OpenSecurityDescriptor : public PublicSecurityDescriptor
		{
		public:
			OpenSecurityDescriptor();
			DWORD GetError() const;

		private:
			DWORD	m_dwError;
		};

		//////////////////////////////////////////////////////////////////////
		// Obtain the logged on SID.
		//////////////////////////////////////////////////////////////////////
		class LoggedOnSid
		{
		public:
			LoggedOnSid();
			~LoggedOnSid();

			void FreeToken();
			void FreeSid();

			DWORD GetCurrentProcessUserTokens();
			DWORD GetLogOnAccountSID();

			PTOKEN_USER GetTokenUser() const;
			PSID GetPSid() const;

		protected:
			PTOKEN_USER m_pTokenUser;
			PSID        m_pSid;
		};

		class LoggedOnSid2 : public LoggedOnSid
		{
		public:
			LoggedOnSid2();
			DWORD GetError() const;

		protected:
			DWORD m_dwError;
		};

		extern OpenSecurityDescriptor g_openSd;
		extern LoggedOnSid2           g_loggedOnSid;
	}
}

//////////////////////////////////////////////////////////////////////////
// Macro to get the logged on sid, get a public security descriptor and
// public security attributes associated with the public security 
// descriptor. 
//////////////////////////////////////////////////////////////////////////
#define GETLOGGEDONSID()   IAMC::Security::g_loggedOnSid.GetPSid()
#define GETPUBLICPSD()     IAMC::Security::g_openSd.GetPSD()
#define GETPUBLICPSA()     IAMC::Security::g_openSd.GetPSA()
