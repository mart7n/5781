#pragma once

#include <windows.h>
#include <tchar.h>
#include <string>
#include <aclapi.h>

namespace NTSecurity
{
	typedef LPVOID PACE;

	/**
	****************************************************************************
	* Access Control Entries (ACE) - An access control entry specifies
	* access to an NT resource.  It contains exactly one SID, an access mask 
	* which specifies what type of access is applied to the resource.  An ACE 
	* can be one of the following types:
	*		access allowed type -	implying access is allowed to the resource 
	*								(or restricted),
	*		access denied type	-	implying access is denied,
	*		system audit		-	for auditing events (such as when someone
	*								attempted to gain access to a resource).
	****************************************************************************
	*/
	class CACE
	{
	public:
		CACE(const PACE = NULL);	
		CACE(const CACE &);
		~CACE();

		PACE operator=(const PACE);	
		CACE & operator=(const CACE &);

		operator PACE();
		operator const PACE() const;

		PSID GetSID() const;
		BYTE GetType() const;		
		DWORD GetSize() const;
		BYTE GetControlFlags() const;
		ACCESS_MASK GetAccessMask() const;

		bool IsSystemAudit() const;	
		bool IsDeniedAccess() const;	
		bool IsAllowedAccess() const;	

		void SetSID(const PSID);
		void SetAllowedAccess();
		void SetDeniedAccess();

		// Note the SUCCESSFUL_ACCESS_ACE_FLAG and FAILED_ACCESS_ACE_FLAG as 
		// specified in the ace's CONTROL FLAGS when creating system audit aces.
		void SetSystemAudit();
		void SetType(BYTE);

		/*
		*************************************************************************
		* 
		* CONTROL FLAGS for the ACE
		*
		* Constant(s)			    Meaning
		* -----------------------------------------------------------------------
		* CONTAINER_INHERIT_ACE     Child objects that are containers, such as 
		*                               directories, inherit the ACE as an 
		*                               effective ACE. The inherited ACE is 
		*                               inheritable unless the 
		*                               NO_PROPAGATE_INHERIT_ACE bit flag is also 
		*                               set.  
		* FAILED_ACCESS_ACE_FLAG    Used with system-audit ACEs in a SACL to 
		*                               generate audit messages for failed access 
		*                               attempts. 
		* INHERIT_ONLY_ACE          Indicates an inherit-only ACE which does not 
		*                               control access to the object to which it 
		*                               is attached. If this flag is not set, the 
		*                               ACE is an effective ACE which controls 
		*                               access to the object to which it is 
		*                               attached. 
		*                               Both effective and inherit-only ACEs can 
		*                               be inherited depending on the state of 
		*                               the other inheritance flags. 
		* NO_PROPAGATE_INHERIT_ACE  If the ACE is inherited by a child object, 
		*                               the system clears the OBJECT_INHERIT_ACE 
		*                               and CONTAINER_INHERIT_ACE flags in the 
		*                               inherited ACE. This prevents the ACE from 
		*                               being inherited by subsequent generations 
		*                               of objects.  
		* OBJECT_INHERIT_ACE        Noncontainer child objects inherit the ACE as 
		*                               an effective ACE. 
		*                               For child objects that are containers, 
		*                               the ACE is inherited as an inherit-only 
		*                               ACE unless the NO_PROPAGATE_INHERIT_ACE 
		*                               bit flag is also set.
		* SUCCESSFUL_ACCESS_ACE_FLAG Used with system-audit ACEs in a SACL to 
		*                               generate audit messages for successful 
		*                               access attempts. 
		*************************************************************************
		*/
		void SetControlFlags(BYTE);

		/*
		*************************************************************************
		* Sets up the access mask for the access control entry.  Access masks 
		* are bit fields which contain generic and specific rights.
		*************************************************************************
		*
		* STANDARD access masks
		*
		* Constant(s)				Access				
		* -----------------------------------------------------------------------
		* DELETE                    The right to delete the object. 
		* READ_CONTROL              The right to read the information in the 
		*                               object's security descriptor, not 
		*                               including the information in the SACL. 
		* SYNCHRONIZE               The right to use the object for 
		*                               synchronization. This enables a thread to 
		*                               wait until the object is in the signaled 
		*                               state. Some object types do not support 
		*                               this access right. 
		* WRITE_DAC                 The right to modify the DACL in the object's 
		*                               security descriptor. 
		* WRITE_OWNER               The right to change the owner in the object's 
		*                               security descriptor. 
		* STANDARD_RIGHTS_ALL       Combines DELETE, READ_CONTROL, WRITE_DAC, 
		*                               WRITE_OWNER, and SYNCHRONIZE access. 
		* STANDARD_RIGHTS_EXECUTE   Currently defined to equal READ_CONTROL. 
		* STANDARD_RIGHTS_READ      Currently defined to equal READ_CONTROL. 
		* STANDARD_RIGHTS_REQUIRED  Combines DELETE, READ_CONTROL, WRITE_DAC, 
		*                               and WRITE_OWNER access. 
		* STANDARD_RIGHTS_WRITE     Currently defined to equal READ_CONTROL. 
		*
		*************************************************************************
		*
		* Typical FILE access masks (see permissions from a file security dialog)
		*
		* Constant(s)				Access				Meaning
		* -----------------------------------------------------------------------
		* FILE_GENERIC_READ |		Change(RWXD)		Read, Write, Execute, 	
		*	FILE_GENERIC_WRITE |							Delete
		*	FILE_GENERIC_EXECUTE | 
		*	DELETE					
		* FILE_GENERIC_READ |		Read(RX)			Read, Execute
		*	FILE_GENERIC_WRITE
		* FILE_GENERIC_READ			Special Access(R)	Read	
		* FILE_GENERIC_WRITE		Special Access(W)	Write
		* FILE_GENERIC_EXECUTE		Special Access(X)	Execute
		* DELETE					Special Access(D)	Delete
		* WRITE_DAC					Special Access(P)	Change Permissions
		* WRITE_OWNER				Special Access(0)	Take Ownership
		* FILE_ALL_ACCESS			Full Control(all)	Full Control
		*
		* Note that when using access denied types with files, the access mask
		* should be set to FILE_ALL_ACCESS.
		*
		*************************************************************************
		*
		* REGISTRY access masks
		*
		* Constant(s)				Access				
		* -----------------------------------------------------------------------
		* KEY_CREATE_LINK           Permission to create a symbolic link. 
		* KEY_CREATE_SUB_KEY        Permission to create subkeys. 
		* KEY_ENUMERATE_SUB_KEYS    Permission to enumerate subkeys. 
		* KEY_EXECUTE               Permission for read access. 
		* KEY_NOTIFY                Permission for change notification. 
		* KEY_QUERY_VALUE           Permission to query subkey data. 
		* KEY_SET_VALUE             Permission to set subkey data. 
		* KEY_ALL_ACCESS            Combines the KEY_QUERY_VALUE, 
		*                               KEY_ENUMERATE_SUB_KEYS, KEY_NOTIFY, 
		*                               KEY_CREATE_SUB_KEY, KEY_CREATE_LINK, and 
		*                               KEY_SET_VALUE access rights, plus all the 
		*                               standard access rights except SYNCHRONIZE.  
		* KEY_READ                  Combines the STANDARD_RIGHTS_READ, 
		*                               KEY_QUERY_VALUE, KEY_ENUMERATE_SUB_KEYS, 
		*                               and KEY_NOTIFY access rights. 
		* KEY_WRITE                 Combines the STANDARD_RIGHTS_WRITE, 
		*                               KEY_SET_VALUE, and KEY_CREATE_SUB_KEY 
		*                               access rights. 
		*************************************************************************
		*
		* PROCESS access masks
		*
		* Any combination of the following access flags can be specified in 
		* addition to the STANDARD_RIGHTS_REQUIRED access flags.
		*
		* Constant(s)				Access				Meaning
		* -----------------------------------------------------------------------
		* PROCESS_ALL_ACCESS        Specifies all possible access flags for the 
		*                               process object. 
		* PROCESS_CREATE_PROCESS    Used internally. 
		* PROCESS_CREATE_THREAD     Enables using the process handle in the 
		*                               CreateRemoteThread function to create a 
		*                               thread in the process. 
		* PROCESS_DUP_HANDLE        Enables using the process handle as either 
		*                               the source or target process in the 
		*                               DuplicateHandle function to duplicate a 
		*                               handle. 
		* PROCESS_QUERY_INFORMATION Enables using the process handle in the 
		*                               GetExitCodeProcess and GetPriorityClass 
		*                               functions to read information from the 
		*                               process object. 
		* PROCESS_SET_QUOTA         Enables using the process handle in the 
		*                               AssignProcessToJobObject and 
		*                               SetProcessWorkingSetSize functions to set 
		*                               memory limits.  
		* PROCESS_SET_INFORMATION   Enables using the process handle in the 
		*                               SetPriorityClass function to set the 
		*                               priority class of the process. 
		* PROCESS_TERMINATE         Enables using the process handle in the 
		*                                   TerminateProcess function to  
		*                                   terminate the process. 
		* PROCESS_VM_OPERATION      Enables using the process handle in the 
		*                                   VirtualProtectEx and  
		*                                   WriteProcessMemory functions to 
		*                                   modify the virtual memory of the 
		*									process. 
		* PROCESS_VM_READ           Enables using the process handle in the 
		*                                   ReadProcessMemory function to read 
		*                                   from the virtual memory of the 
		*									process. 
		* PROCESS_VM_WRITE          Enables using the process handle in the 
		*                                   WriteProcessMemory function to write
		*                                   to the virtual memory of the process. 
		* SYNCHRONIZE               Enables using the process handle in any of 
		*                                   the wait functions to wait for the 
		*                                   process to terminate. 
		*************************************************************************
		*/
		void SetAccessMask(ACCESS_MASK);

		static DWORD GetSizeACE(const PACE);

		bool IsValid(LPCTSTR = NULL) const;

		std::basic_string<TCHAR> Dump() const;
		std::basic_string<TCHAR> DumpXML() const;

	private:
		void Free();	

		PACE m_pAce;
	};
}
