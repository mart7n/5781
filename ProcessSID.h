#pragma once

#include <windows.h>
#include <tchar.h>
#include <aclapi.h>
#include "SID.h"

namespace NTSecurity
{
	class CProcessSID
	{
	public:
		CProcessSID();

		PSID GetSID();
		const PSID GetSID() const;

		operator PSID();
		operator const PSID() const;
		
	private:
		CSID	m_sid;		
		PSID	m_pSID;
	};

	PSID GetProcessSID();
}
