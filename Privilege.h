#pragma once

#include <windows.h>
#include <tchar.h>
#include <string>
#include <aclapi.h>

namespace NTSecurity
{
	/**
	****************************************************************************
	* Privilege class (which encapsulates LUIDs).  See below for list of
	* NT privileges.
	****************************************************************************
	*/
	class CPrivilege
	{
	public:
		CPrivilege(const PLUID = NULL);
		CPrivilege(const CPrivilege &);
		CPrivilege(LPCTSTR, LPCTSTR = NULL);
				
		PLUID operator=(const PLUID);
		CPrivilege & operator=(const CPrivilege &);

		operator PLUID();
		bool CreateUnique();

		/*
		*************************************************************************
		* NT defined Privileges - the following is an extract from the Windows 
		* NT 3.51 resource guide on user rights.
		*
		* SeTcbPrivilege
		*  Act as part of the operating system
		*  The user can use to perform as a secure, trusted part of the operating 
		*  system. Some subsystems are granted this privilege.
		* Granted by default: None
		*  
		* SeChangeNotifyPrivilege
		*  Bypass traverse checking
		*  The user can traverse directory trees. Deny access to users using 
		*  POSIX applications.
		* Granted by default: Everyone
		*  
		* SeCreatePagefilePrivilege
		*  Create a pagefile
		*  The user can create a page file (not available in this version of 
		* Windows NT). Security is determined by a users access to the 
		* ..\CurrentControlSet\Control\Session Management key. 
		* Granted by default: None
		*  
		* SeCreateTokenPrivilege
		*  Create a token object
		*  Required to create access tokens. Only the Local Security Authority 
		* can do this.
		* Granted by default: None
		*  
		* SeCreatePermanentPrivilege
		*  Create permanent shared objects
		*  Required to create special permanent objects, such as \\Device, 
		*  which are used within Windows NT. 
		* Granted by default: None
		*  
		* SeDebugPrivilege
		*  Debug programs
		*  The user can debug various low-level objects such as threads.
		* Granted by default: Administrators
		*  
		* SeAuditPrivilege
		*  Generate security audits
		*  Required to generate security audit log entries.
		* Granted by default: None
		* 
		* SeIncreaseQuotaPrivilege
		*  Increase quotas
		*  Required to increase object quotas (not available in this version of
		*  Windows NT). 
		* Granted by default: None
		*  
		* SeIncreaseBasePriorityPrivilege
		*  Increase scheduling priority
		*  The user can boost the priority of a process.
		* Granted by default: Administrators and Power Users
		*  
		* SeLoadDriverPrivilege
		*  Load and unload device drivers
		*  The user can load an unload device drivers.
		* Granted by default: Administrators
		*  
		* SeLockMemoryPrivilege
		*  Lock pages in memory
		*  The user can lock pages in memory so they cannot be paged out to a 
		*  backing store such as PAGEFILE.SYS. As physical memory is a limited 
		*  resource, locking pages can lead to greater disk thrashing as 
		*  essentiallythe amount of physical pages available to other 
		*  applications is  reduced.
		* Granted by default: None
		*  
		* No Name
		*  Log on as a batch job
		*  The user can log on using a batch queue facility (not available in 
		*  this version of Windows NT). 
		* Granted by default: None
		*  
		* No Name
		*  Log on as a service
		*  The user can perform security services.
		* Granted by default: None
		* 
		* SeSystemEnvironmentPrivilege
		*  Modify Firmware environment variables
		*  The user can modify system environment variables (not user environment
		*  variables).
		* Granted by default: Administrators
		*  
		* SeProfileSingleProcessPrivilege
		*  Profile single process
		*  The user can use the profiling (performance sampling) capabilities of 
		*  Windows NT on a process.
		* Granted by default: Administrators and Power Users
		*  
		* SeSystemProfilePrivilege
		*  Profile system performance
		*  The user can use the profiling capabilities of Windows NT on the 
		*  system. (This can slow the system down.)
		* Granted by default: Administrators
		*  
		* SeAssignPrimaryTokenPrivilege
		*  Replace a process level token 
		*  Required to modify a process's security access token. This is a 
		*  powerful privilege used only by the system.
		* Granted by default: None
		*  
		*************************************************************************
		*/

		/*
		*************************************************************************
		* sets the privilege by name.  This must be one of the following strings
		* (or constants) as defined in winnt.h.
		*
		* Define                            String equivalent
		*************************************************************************
		* SE_CREATE_TOKEN_NAME              "SeCreateTokenPrivilege"
		* SE_ASSIGNPRIMARYTOKEN_NAME        "SeAssignPrimaryTokenPrivilege"
		* SE_LOCK_MEMORY_NAME               "SeLockMemoryPrivilege"
		* SE_INCREASE_QUOTA_NAME            "SeIncreaseQuotaPrivilege"
		* SE_UNSOLICITED_INPUT_NAME         "SeUnsolicitedInputPrivilege"
		* SE_MACHINE_ACCOUNT_NAME           "SeMachineAccountPrivilege"
		* SE_TCB_NAME                       "SeTcbPrivilege"
		* SE_SECURITY_NAME                  "SeSecurityPrivilege"
		* SE_TAKE_OWNERSHIP_NAME            "SeTakeOwnershipPrivilege"
		* SE_LOAD_DRIVER_NAME               "SeLoadDriverPrivilege"
		* SE_SYSTEM_PROFILE_NAME            "SeSystemProfilePrivilege"
		* SE_SYSTEMTIME_NAME                "SeSystemtimePrivilege"
		* SE_PROF_SINGLE_PROCESS_NAME       "SeProfileSingleProcessPrivilege"
		* SE_INC_BASE_PRIORITY_NAME         "SeIncreaseBasePriorityPrivilege"
		* SE_CREATE_PAGEFILE_NAME           "SeCreatePagefilePrivilege"
		* SE_CREATE_PERMANENT_NAME          "SeCreatePermanentPrivilege"
		* SE_BACKUP_NAME                    "SeBackupPrivilege"
		* SE_RESTORE_NAME                   "SeRestorePrivilege"
		* SE_SHUTDOWN_NAME                  "SeShutdownPrivilege"
		* SE_DEBUG_NAME                     "SeDebugPrivilege"
		* SE_AUDIT_NAME                     "SeAuditPrivilege"
		* SE_SYSTEM_ENVIRONMENT_NAME        "SeSystemEnvironmentPrivilege"
		* SE_CHANGE_NOTIFY_NAME             "SeChangeNotifyPrivilege"
		* SE_REMOTE_SHUTDOWN_NAME           "SeRemoteShutdownPrivilege"
		*
		*************************************************************************
		*/
		bool SetName(LPCTSTR, LPCTSTR);

		// Obtain privilege name 		
		bool GetName(LPCTSTR, std::basic_string<TCHAR> &) const;

		std::basic_string<TCHAR> Dump() const;
		std::basic_string<TCHAR> DumpXML() const;

	private:
		LUID m_luid;
	};
}
