#include <assert.h>
#include "Strtok.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CStrTok members.
//////////////////////////////////////////////////////////////////////////////
CStrTok::CStrTok(LPCTSTR pszParams, LPCTSTR pszData) : m_pszNext(NULL)
{
    if (pszData)
    {
        m_strBuffer.append(pszData);
        m_strBuffer.append(_T("\0"), 1);

        for(LPTSTR pszMem=(LPTSTR)m_strBuffer.c_str(); *pszMem; 
			pszMem++)
        {
            LPCTSTR pszChar = ::_tcschr(pszParams, *pszMem);
            if (pszChar)
                *pszMem = _T('\0');
        }

        m_pszNext = m_strBuffer.c_str();       
    }
}

LPCTSTR CStrTok::GetNext()
{
	if (!m_pszNext)
		return NULL;

	if (m_pszNext == m_strBuffer.c_str() + m_strBuffer.size())
        return NULL;

    LPCTSTR pszResult = m_pszNext;
    if (!(*m_pszNext))
        m_pszNext++;
    else
        m_pszNext = (LPCTSTR)::memchr(m_pszNext+1, NULL, MAXDWORD) + sizeof(TCHAR);
    
    return pszResult;
}
