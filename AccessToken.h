#pragma once

#include <windows.h>
#include <tchar.h>
#include <string>
#include <aclapi.h>
#include "ACL.h"
#include "SID.h"
#include "Privilege.h"

namespace NTSecurity
{
	/**
	****************************************************************************
	* Access Token 
	* An access token is an object that describes the security context of a 
	* process or thread. The information in a token includes the identity and 
	* privileges of the user account associated with the process or thread. When
	* a user logs on, the system verifies the user's password by comparing it 
	* with information stored in a security database. If the password is 
	* authenticated, the system produces an access token. Every process executed
	* on behalf of this user has a copy of this access token.
	* 
	* The system uses an access token to identify the user when a thread 
	* interacts with a securable object or tries to perform a system task that 
	* requires privileges. Access tokens contain the following information: 
	* 
	*  - The security identifier (SID) for the user's account,
	*  - SIDs for the groups of which the user is a member,
	*  - A logon SID that identifies the current logon session,
	*  - A list of the privileges held by either the user or the user's groups 
	*  - An owner SID 
	*  - The SID for the primary group 
	*  - The default DACL that the system uses when the user creates a 
	*    securable object without specifying a security descriptor 
	*  - The source of the access token 
	*  - Whether the token is a primary or impersonation token 
	*  - An optional list of restricting SIDs 
	*  - Current impersonation levels 
	*  - Other statistics 
	* 
	* Every process has a primary token that describes the security context of 
	* the user account associated with the process. By default, the system uses 
	* the primary token when a thread of the process interacts with a securable 
	* object. However, a thread can impersonate a client account. This allows 
	* the thread to interact with securable objects using the client's security 
	* context. A thread that is impersonating a client has both a primary token 
	* and an impersonation token. Use the OpenProcessToken function to retrieve 
	* a handle to the primary token of a process. Use the OpenThreadToken 
	* function to retrieve a handle to the impersonation token of a thread. 
	* For more information, see Impersonation in the MSDN. 
	****************************************************************************
	*/
	class CAccessToken
	{
	public:		
		CAccessToken(const HANDLE = NULL);
		~CAccessToken();

		HANDLE operator=(HANDLE);
		operator HANDLE();

		/*
		*************************************************************************
		* Obtain the default DACL for newly created objects. 
		* TOKEN_QUERY access required on access token for this function.
		*************************************************************************
		*/
		PACL GetDefaultDACL();

		/*
		*************************************************************************
		* Obtains pointer to a TOKEN_GROUPS structure containing the group 
		* accounts associated with the token.  This can be passed into a
		* CTokenGroups object for manipulation.
		* TOKEN_QUERY access required on access token for this function.
		*************************************************************************
		*/
		PTOKEN_GROUPS GetGroups();

		typedef SECURITY_IMPERSONATION_LEVEL	Impersonation;
		typedef TOKEN_TYPE                      Type;

		/*
		*************************************************************************
		* obtain type which indicate whether the token is a primary or 
		* impersonation token. 
		* TOKEN_QUERY access required on access token for this function.
		*************************************************************************
		*/
		Type GetType() const;
		bool IsPrimaryType() const;
		bool IsImpersonationType() const;
		
		/*
		*************************************************************************
		* Obtain enum indicating the impersonation level of the token
		*
		* This will be one of the following:
		*
		* Constant                  Meaning
		*************************************************************************
		* SecurityAnonymous         The server process cannot obtain 
		*                           identification information about the client 
		*                           and it cannot impersonate the client. It is 
		*                           defined with no value given, and thus, by 
		*                           ANSI C rules, defaults to a value of 0. 
		* SecurityIdentification    The server process can obtain information 
		*                           about the client, such as security 
		*                           identifiers and privileges, but it cannot 
		*                           impersonate the client. This is useful for 
		*                           servers that export their own objects...for 
		*                           example, database products that export tables 
		*                           and views. Using the retrieved 
		*                           client-security information, the server can 
		*                           make access-validation decisions without 
		*							being able to utilize other services using 
		*                           the client's security context. 
		* SecurityImpersonation     The server process can impersonate the 
		*                           client's security context on its local system. 
		*                           The server cannot impersonate the client on 
		*                           remote systems. 
		* SecurityDelegation        Windows NT/Windows 2000 security does not 
		*                           support this impersonation level. 
		*
		* TOKEN_QUERY access required on access token for this function.
		*************************************************************************
		*/
		Impersonation GetImpersonationLevel() const;

		/*
		*************************************************************************
		* Obtain default owner SID that will be applied to newly created objects. 
		* TOKEN_QUERY access required on access token for this function.
		*************************************************************************
		*/
		PSID GetOwner();

		/*
		*************************************************************************
		* Obtain the SID which identifies the user associated with the access 
		* token.
		* TOKEN_QUERY access required on access token for this function.
		*************************************************************************
		*/
		PSID GetUser();

		/*
		*************************************************************************
		* obtain group security identifier (SID) for an access token. 
		* TOKEN_QUERY access required on access token for this function.
		*************************************************************************
		*/
		PSID GetPrimaryGroup();

		/*
		*************************************************************************
		* Obtain token privileges for the access token.  This can be passed into
		* a CTokenPrivileges object for manipluation.
		* TOKEN_QUERY access required on access token for this function.
		*************************************************************************
		*/
		PTOKEN_PRIVILEGES GetPrivileges();

		/*
		*************************************************************************
		* Obtains pointer to a TOKEN_GROUPS structure containing the restricting
		* accounts (sids) associated with the token.  This can be passed into a
		* CTokenGroups object for manipulation.
		* TOKEN_QUERY access required on access token for this function.
		*************************************************************************
		*/
		PTOKEN_GROUPS GetRestrictingSids();

		/*
		*************************************************************************
		* Returns a DWORD value that indicates the Terminal Services session 
		* identifier associated with the token. If the token is associated with 
		* the Terminal Server console session, the session identifier is zero. 
		* A nonzero session identifier indicates a Terminal Services client 
		* session. In a non-Terminal Services environment, the session identifier
		* is zero.
		* TOKEN_QUERY access required on access token for this function.
		*************************************************************************
		*/
		DWORD GetSessionId() const;

		/*
		*************************************************************************
		* Obtains pointer to the TOKEN_SOURCE structure.  This structure 
		* identifies the source of an access token.  The structure contains the
		* following:
		*   SourceName - Specifies an 8-byte character string used to identify 
		*       the source of an access token. This is used to distinguish 
		*       between such sources as Session Manager, LAN Manager, and RPC 
		*       Server. A string, rather than a constant, is used to identify 
		*       the source so users and developers can make extensions to the 
		*       system, such as by adding other networks, that act as the source 
		*       of access tokens. 
		*   SourceIdentifier - Specifies a locally unique identifier (LUID) 
		*       provided by the source component named by the SourceName member. 
		*       This value aids the source component in relating context blocks, 
		*       such as session-control structures, to the token. This value is 
		*       typically, but not necessarily, an LUID. 
		*
		* TOKEN_QUERY_SOURCE access required on access token for this function.
		*************************************************************************
		*/
		PTOKEN_SOURCE GetSource();

		/*
		*************************************************************************
		* Obtain general information about an access token. 
		* TOKEN_QUERY access required on access token for this function.
		*************************************************************************
		*/
		PTOKEN_STATISTICS GetStatistics();

		/*
		*************************************************************************
		* Set default owner SID that will be applied to newly created objects. 
		* TOKEN_ADJUST_DEFAULT access required on access token for this function.
		*************************************************************************
		*/
		bool SetOwner(const PSID);

		/*
		*************************************************************************
		* Set the primary group sid for the access token.  The PSID parameter 
		* represents a group that will become the primary group of any objects 
		* created by a process using this access token. The security identifier 
		* (SID) must be one of the group SIDs already in the token. 
		* TOKEN_ADJUST_DEFAULT access required on access token for this function.
		*************************************************************************
		*/
		bool SetPrimaryGroup(const PSID);

		/*
		*************************************************************************
		* Set the default discretionay access control list for the access token,
		* The ACL structure provided as a new default discretionary ACL is not 
		* validated for correctness or consistency. If the access control list 
		* parameter is NULL, the current default discretionary ACL is removed and 
		* no replacement is established.
		*************************************************************************
		*/
		bool SetDefaultDACL(const PACL);

		/*
		*************************************************************************
		* Adjusts groups in the access token. 
		* Mandatory groups cannot be disabled. They are identified by the 
		* SE_GROUP_MANDATORY attribute in the TOKEN_GROUPS structure. If an 
		* attempt is made to disable any mandatory groups, AdjustTokenGroups 
		* fails and leaves the state of all groups unchanged.
		*
		* TOKEN_ADJUST_GROUPS access is required to enable or disable groups in 
		* an access token. 
		*************************************************************************
		*/
		bool SetGroups(const PTOKEN_GROUPS);

		/*
		*************************************************************************
		* Resets groups for the access token to their default enabled and 
		* disabled states. 
		* 
		* The CTokenGroups class can be used to manipulate the 
		* TOKEN_GROUPS structure.
		*************************************************************************
		*/
		bool ResetGroups();

		/*
		*************************************************************************
		* Enables or disables privileges in the access token. 
		*
		* New privileges CANNOT be added to the access token. The access tokens 
		* existing privileges can only be enabled or disabled.
		*
		* The CTokenPrivileges class can be used to manipulate the 
		* TOKEN_PRIVILEGES structure.
		*
		* Enabling or disabling privileges in an access token requires 
		* TOKEN_ADJUST_PRIVILEGES access. 
		*************************************************************************
		*/
		bool AdjustPrivileges(const PTOKEN_PRIVILEGES);

		/*
		*************************************************************************
		* Disable ALL privileges within an access token. 
		*
		* Enabling or disabling privileges in an access token requires 
		* TOKEN_ADJUST_PRIVILEGES access.
		*************************************************************************
		*/
		bool DisableAllPrivileges();

		/*
		*************************************************************************
		* Methods for enabling or disabling privileges - see notes in 
		* Privilege class for names of privileges and their meanings.
		*************************************************************************
		*/		
		bool EnablePrivilege(LPCTSTR);
		bool DisablePrivilege(LPCTSTR);
		bool EnablePrivilege(const PLUID);
		bool DisablePrivilege(const PLUID);
		
		bool IsPrivilegeEnabled(LPCTSTR);
		bool IsPrivilegeInToken(LPCTSTR);
		bool IsPrivilegeEnabled(const PLUID);
		bool IsPrivilegeInToken(const PLUID);

		bool IsSIDEnabled(const PSID);

		/*
		*************************************************************************
		* Create the access token from a specific user - the access token can 
		* then be passed to ::ImpersonateLoggedOnUser() for impersonation
		*************************************************************************
		*/
		bool CreateFromUser(LPCTSTR, LPCTSTR, LPCTSTR);

		std::basic_string<TCHAR> Dump();
		std::basic_string<TCHAR> DumpXML();

	private:
		/*
		*************************************************************************
		*  Enables or disables a privilege (which must exist in the access 
		* token).  
		* Enabling or disabling privileges in an access token requires 
		* TOKEN_ADJUST_PRIVILEGES access. 
		*************************************************************************
		*/
		bool SetPrivilege(const PLUID, bool);
		bool FindPrivilege(const PLUID, LUID_AND_ATTRIBUTES * &);

		CSID				m_sidUser;
		CACL				m_defDacl;
		CSID				m_sidOwner;
		CSID				m_sidPrimaryGroup;		

		PTOKEN_PRIVILEGES   m_pPrivs;
		PTOKEN_GROUPS       m_pGroups;    
		PTOKEN_GROUPS       m_pRestrictSids;

		HANDLE				m_hToken;		
		TOKEN_STATISTICS    m_tokenStats;
		TOKEN_SOURCE        m_tokenSource;
	};
}
