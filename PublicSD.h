#pragma once

#include <windows.h>
#include <aclapi.h>

namespace NTSecurity
{
	class CPublicSD
	{
	public:
		CPublicSD();
		~CPublicSD();

		PSECURITY_DESCRIPTOR GetSD() const;

	private:
		void Clear();

		PSECURITY_DESCRIPTOR m_pSD;
		PACL                 m_pACL;
		PSID                 m_pAdminSID;
		PSID                 m_pEveryoneSID;
	};
}
