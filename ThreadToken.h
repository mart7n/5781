#pragma once

#include <windows.h>
#include <tchar.h>
#include <aclapi.h>

namespace NTSecurity
{
	class CThreadToken
	{
	public:
		CThreadToken(
			bool bProcessContext = false,
			DWORD dwAccess = TOKEN_QUERY,
			HANDLE = ::GetCurrentThread());

		~CThreadToken();

		operator HANDLE();
		HANDLE GetHANDLE();

	private:
		HANDLE m_hToken;
	};
}