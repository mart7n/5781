#include <assert.h>
#include "HeapAllocate.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CHeapAllocate members.
//////////////////////////////////////////////////////////////////////////////
CHeapAllocate::CHeapAllocate() : m_pMem(nullptr)
{
}

CHeapAllocate::~CHeapAllocate()
{
	Free(m_pMem);
}

void CHeapAllocate::Free()
{
	Free(m_pMem);	
}

CHeapAllocate::CHeapAllocate(size_t nBytes) : m_pMem(nullptr)
{
	Alloc(nBytes);
}

LPVOID CHeapAllocate::Alloc(size_t nBytes)
{
	Free();
	m_pMem = ::HeapAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY, nBytes);
	return m_pMem;
}

LPVOID CHeapAllocate::Detach()
{
	LPVOID pMem = m_pMem;
	m_pMem = nullptr;
	return pMem;
}
