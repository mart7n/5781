#include <assert.h>
#include <atlbase.h>
#include "Privilege.h"
#include "AccessToken.h"
#include "TokenGroups.h"
#include "HeapAllocate.h"
#include "TokenPrivileges.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CAccessToken members.
//////////////////////////////////////////////////////////////////////////////
CAccessToken::CAccessToken(const HANDLE hToken)
  : m_hToken(hToken), 
	m_pGroups(NULL), 
	m_pPrivs(NULL), 
	m_pRestrictSids(NULL)
{
}

HANDLE CAccessToken::operator=(HANDLE hToken)
{
	m_hToken = hToken;
	return m_hToken;
}

CAccessToken::~CAccessToken()
{
	CHeapAllocate::Free(m_pGroups);
	CHeapAllocate::Free(m_pPrivs);
	CHeapAllocate::Free(m_pRestrictSids);
}

CAccessToken::operator HANDLE() 
{ 
	return m_hToken; 
}

bool CAccessToken::IsImpersonationType() const 
{ 
	return GetType() == TokenImpersonation; 
}

bool CAccessToken::IsPrimaryType() const
{ 
	return GetType() == TokenPrimary; 
}

bool CAccessToken::EnablePrivilege(const PLUID pLuid) 
{ 
	return SetPrivilege(pLuid, true); 
}

bool CAccessToken::DisablePrivilege(const PLUID pLuid)
{ 
	return SetPrivilege(pLuid, false); 
}

bool CAccessToken::EnablePrivilege(LPCTSTR pszPrivilege)
{
	CPrivilege priv(pszPrivilege); 
	return SetPrivilege((PLUID)priv, true); 
}

bool CAccessToken::DisablePrivilege(LPCTSTR pszPrivilege)
{
	CPrivilege priv(pszPrivilege);
	return SetPrivilege((PLUID)priv, false);
}

bool CAccessToken::IsPrivilegeInToken(LPCTSTR pszPrivilege)
{
	CPrivilege priv(pszPrivilege);
	LUID_AND_ATTRIBUTES * pPrivilege;
	return FindPrivilege((PLUID)priv, pPrivilege);
}

bool CAccessToken::IsPrivilegeInToken(const PLUID pLuid)
{
	LUID_AND_ATTRIBUTES * pPrivilege;
	return FindPrivilege(pLuid, pPrivilege);
}

bool CAccessToken::IsPrivilegeEnabled(LPCTSTR pszPrivilege)
{
	CPrivilege priv(pszPrivilege);
	return IsPrivilegeEnabled((PLUID)priv);
}

bool CAccessToken::IsPrivilegeEnabled(const PLUID pLuid)
{
	LUID_AND_ATTRIBUTES * pPrivilege;
	if (FindPrivilege(pLuid, pPrivilege))
		return (pPrivilege->Attributes & SE_PRIVILEGE_ENABLED) != 0;

	return false;
}

 /**
 ****************************************************************************
 * Obtain the default dacl for the access token (TOKEN_QUERY access 
 * required on the access token handle).
 ****************************************************************************
 */
PACL CAccessToken::GetDefaultDACL()
{  
	if (!m_hToken)
		return NULL;

	DWORD cbInfo = 0;
	PACL pResult = NULL;
	::GetTokenInformation(m_hToken, TokenDefaultDacl, NULL, NULL, &cbInfo);

	if (cbInfo)
	{
		CHeapAllocate allocator;
		TOKEN_DEFAULT_DACL * pInfo = (TOKEN_DEFAULT_DACL *)allocator.Alloc(cbInfo);
		if (::GetTokenInformation(m_hToken, TokenDefaultDacl, pInfo, 
			cbInfo, &cbInfo))
		{
			m_defDacl = pInfo->DefaultDacl;
			pResult = (PACL)m_defDacl;
		}
	}

	return pResult;
}

 /**
 ****************************************************************************
 * Obtains pointer to a TOKEN_GROUPS structure containing the group 
 * accounts associated with the token.  This can be passed into a
 * CTokenGroups object for manipulation.
 ****************************************************************************
 */
PTOKEN_GROUPS CAccessToken::GetGroups()
{
	if (!m_hToken)
		return NULL;

	DWORD cbInfo = 0;
	::GetTokenInformation(m_hToken, TokenGroups, NULL, NULL, &cbInfo);

	if (cbInfo)
	{              
		CHeapAllocate::Free(m_pGroups);		

		CHeapAllocate allocator(cbInfo);
		m_pGroups = (PTOKEN_GROUPS)allocator.Detach();
		if (::GetTokenInformation(m_hToken, TokenGroups, 
			m_pGroups, cbInfo, &cbInfo))
		{
			return m_pGroups;
		}
	}

	return NULL;
}

 /**
 ****************************************************************************
 * Returns type of token (as dicated by TOKEN_TYPE enumeration). If
 * the access token is NULL or an invalid handle, then the return value is
 * undefined. 
 ****************************************************************************
 */
CAccessToken::Type CAccessToken::GetType() const 
{
	Type tokenType;

	if (m_hToken)
	{        
		DWORD cbInfo = sizeof(Type);
		::GetTokenInformation(m_hToken, TokenType, &tokenType, cbInfo, &cbInfo);
	}

	return tokenType;
}

 /**
 ****************************************************************************
 * Obtain impersonation level of the access token.  If the access token  
 * is NULL or the token is not an impersonation type (see 
 * IsImpersonationToken()) then the return value will be undefined.
 ****************************************************************************
 */
CAccessToken::Impersonation CAccessToken::GetImpersonationLevel() const 
{
	Impersonation tokenImpersonate;

	if (m_hToken && IsImpersonationType())
	{        
		DWORD cbInfo = sizeof(Impersonation);
		::GetTokenInformation(m_hToken, TokenImpersonationLevel, 
			&tokenImpersonate, cbInfo, &cbInfo);
	}

	return tokenImpersonate;
}

 /**
 ****************************************************************************
 * Obtain default owner CSID that will be applied to newly created 
 * objects. NULL will be returned if the access token is NULL.
 ****************************************************************************
 */
PSID CAccessToken::GetOwner()
{    
	PSID pResult = NULL;

	if (m_hToken)
	{        		
		DWORD cbInfo = 0;
		::GetTokenInformation(m_hToken, TokenOwner, NULL, NULL, &cbInfo);

		if (cbInfo)
		{
			CHeapAllocate allocator;
			TOKEN_OWNER * pInfo = (TOKEN_OWNER*)allocator.Alloc(cbInfo);
			if (::GetTokenInformation(m_hToken, TokenOwner, pInfo, 
				cbInfo, &cbInfo))
			{
				m_sidOwner = (PSID)pInfo->Owner;
				pResult = (PSID)m_sidOwner;
			}
		}
	}

	return pResult;
}

 /**
 ****************************************************************************
 * Obtain the CSID which identifies the user associated with the access 
 * token.
 ****************************************************************************
 */
PSID CAccessToken::GetUser()
{    
	PSID pResult = NULL;

	if (m_hToken)
	{        		
		DWORD cbInfo = 0;
		::GetTokenInformation(m_hToken, TokenUser, NULL, NULL, &cbInfo);

		if (cbInfo)
		{
			CHeapAllocate allocator;
			TOKEN_USER * pInfo = (TOKEN_USER*)allocator.Alloc(cbInfo);
			if (::GetTokenInformation(m_hToken, TokenUser, pInfo, 
				cbInfo, &cbInfo))
			{
				m_sidUser = (PSID)pInfo->User.Sid;
				pResult = (PSID)m_sidUser;
			}
		}
	}

	return pResult;
}

 /**
 ****************************************************************************
 * Obtain default owner CSID that will be applied to newly created 
 * objects.
 ****************************************************************************
 */
PSID CAccessToken::GetPrimaryGroup()
{
	PSID pResult = NULL;

	if (m_hToken)
	{        		
		DWORD cbInfo = 0;
		::GetTokenInformation(m_hToken, TokenPrimaryGroup, NULL, NULL, &cbInfo);

		if (cbInfo)
		{
			CHeapAllocate allocator;
			TOKEN_PRIMARY_GROUP * pInfo = (TOKEN_PRIMARY_GROUP*)allocator.Alloc(cbInfo);
			if (::GetTokenInformation(m_hToken, TokenPrimaryGroup, pInfo, 
				cbInfo, &cbInfo))
			{
				m_sidPrimaryGroup = (PSID)pInfo->PrimaryGroup;
				pResult = (PSID)m_sidPrimaryGroup;
			}
		}
	}

	return pResult;
}

 /**
 ****************************************************************************
 * Obtains pointer to a PTOKEN_PRIVILEGES structure containing the 
 * privileges associated with the token.  This can be passed into a
 * CTokenPrivileges object for manipulation.
 ****************************************************************************
 */
PTOKEN_PRIVILEGES CAccessToken::GetPrivileges()
{
	if (!m_hToken)
		return NULL;

	DWORD cbInfo = 0;
	::GetTokenInformation(m_hToken, TokenPrivileges, NULL, NULL, &cbInfo);

	if (cbInfo)
	{              
		CHeapAllocate::Free(m_pPrivs);
		
		CHeapAllocate allocator(cbInfo);
		m_pPrivs = (PTOKEN_PRIVILEGES)allocator.Detach();
		if (::GetTokenInformation(m_hToken, TokenPrivileges, 
			m_pPrivs, cbInfo, &cbInfo))
		{            
			return m_pPrivs;
		}

	}

	return NULL;
}

 /**
 ****************************************************************************
 * Obtains pointer to a TOKEN_GROUPS structure containing the restricting
 * accounts (sids) associated with the token.  This can be passed into a
 * CTokenGroups object for manipulation.
 ****************************************************************************
 */
PTOKEN_GROUPS CAccessToken::GetRestrictingSids()
{
	if (!m_hToken)
		return NULL;

	DWORD cbInfo = 0;
	::GetTokenInformation(m_hToken, TokenRestrictedSids, NULL, NULL, &cbInfo);
	if (cbInfo)
	{              		
		CHeapAllocate::Free(m_pRestrictSids);

		CHeapAllocate allocator(cbInfo);
		m_pRestrictSids = (PTOKEN_GROUPS)allocator.Detach();
		if (::GetTokenInformation(m_hToken, TokenRestrictedSids, 
			m_pGroups, cbInfo, &cbInfo))
		{
			return m_pRestrictSids;
		}
	}

	return NULL;
}

 /**
 ****************************************************************************
 * Returns a DWORD value that indicates the Terminal Services session 
 * identifier associated with the token. If the token is associated with the
 * Terminal Server console session, the session identifier is zero. 
 * A nonzero session identifier indicates a Terminal Services client session. 
 * In a non-Terminal Services environment, the session identifier is zero.
 ****************************************************************************
 */
DWORD CAccessToken::GetSessionId() const
{
	if (!m_hToken)
		return 0;

	DWORD dwResult = 0;
	DWORD cbInfo = sizeof(DWORD);
	::GetTokenInformation(m_hToken, TokenSessionId, &dwResult, cbInfo, &cbInfo);
	return dwResult;
}

 /**
 ****************************************************************************
 * Obtains pointer to the TOKEN_SOURCE structure.  This structure 
 * identifies the source of an access token.  The structure contains the
 * following:
 *   SourceName - Specifies an 8-byte character string used to identify 
 *      the source of an access token. This is used to distinguish between 
 *      such sources as Session Manager, LAN Manager, and RPC Server. A 
 *      string, rather than a constant, is used to identify the source so 
 *      users and developers can make extensions to the system, such as by 
 *      adding other networks, that act as the source of access tokens. 
 *   SourceIdentifier - Specifies a locally unique identifier (LUID) 
 *      provided by the source component named by the SourceName member. 
 *      This value aids the source component in relating context blocks, 
 *      such as session-control structures, to the token. This value is 
 *      typically, but not necessarily, an LUID. 
 *
 * TOKEN_QUERY_SOURCE access required on access token for this function.
 ****************************************************************************
 */
PTOKEN_SOURCE CAccessToken::GetSource()
{
	if (!m_hToken)
		return 0;

	DWORD cbInfo = sizeof(TOKEN_SOURCE);
	if (::GetTokenInformation(m_hToken, TokenSource, &m_tokenSource, 
		cbInfo, &cbInfo))
	{
		return &m_tokenSource;
	}

	return NULL;
}

 /**
 ****************************************************************************
 * Obtain general information about an access token. 
 * TOKEN_QUERY access required on access token for this function.
 ****************************************************************************
 */
PTOKEN_STATISTICS CAccessToken::GetStatistics()
{
	if (!m_hToken)
		return 0;

	DWORD cbInfo = sizeof(TOKEN_STATISTICS);    
	if (::GetTokenInformation(m_hToken, TokenStatistics, &m_tokenStats, 
		cbInfo, &cbInfo))
	{
		return &m_tokenStats;
	}

	return NULL;
}

 /**
 ****************************************************************************
 * Set default owner CSID that will be applied to newly created objects. 
 * TOKEN_ADJUST_DEFAULT access required on access token for this function.
 ****************************************************************************
 */
bool CAccessToken::SetOwner(const PSID pSid)
{
	if (!pSid || !m_hToken)
		return false;

	TOKEN_OWNER tok = { pSid };
	return (::SetTokenInformation(m_hToken, TokenOwner, &tok, sizeof(tok)) 
		== TRUE);
}

 /**
 ****************************************************************************
 * Set the primary group sid for the access token.  The PSID parameter 
 * represents a group that will become the primary group of any objects 
 * created by a process using this access token. The security identifier 
 * (CSID) must be one of the group SIDs already in the token. 
 * TOKEN_ADJUST_DEFAULT access required on access token for this function.
 ****************************************************************************
 */
bool CAccessToken::SetPrimaryGroup(
	const PSID pSid)
{
	if (!pSid || !m_hToken)
		return false;

	TOKEN_PRIMARY_GROUP tok = { pSid };
	return (::SetTokenInformation(m_hToken, TokenPrimaryGroup, 
		&tok, sizeof(tok)) == TRUE);
}

 /**
 ****************************************************************************
 * Set the default discretionay access control list for the access token,
 * The ACL structure provided as a new default discretionary ACL is not 
 * validated for correctness or consistency. If the access control list 
 * parameter is NULL, the current default discretionary ACL is removed and 
 * no replacement is established.
 * TOKEN_ADJUST_DEFAULT access required on access token for this function.
 ****************************************************************************
 */
bool CAccessToken::SetDefaultDACL(const PACL pAcl)
{
	if (!m_hToken)
		return false;

	TOKEN_DEFAULT_DACL dacl = { pAcl };
	return (::SetTokenInformation(m_hToken, TokenDefaultDacl, 
		pAcl ? &dacl : NULL, pAcl ? sizeof(dacl) : NULL) == TRUE);
}

 /**
 ****************************************************************************
 * Adjusts groups in the access token. 
 * Mandatory groups cannot be disabled. They are identified by the 
 * SE_GROUP_MANDATORY attribute in the TOKEN_GROUPS structure. If an attempt 
 * is made to disable any mandatory groups, AdjustTokenGroups fails and 
 * leaves the state of all groups unchanged.
 * The CTokenGroups class can be used to manipulate the 
 * TOKEN_GROUPS structure.
 *
 * TOKEN_ADJUST_GROUPS access is required to enable or disable groups in an
 * access token.
 ****************************************************************************
 */
bool CAccessToken::SetGroups(const PTOKEN_GROUPS pGroups)
{
	if (!pGroups || !m_hToken)
		return false;

	DWORD cbGroups = CTokenGroups::GetSize(pGroups);
	return (::AdjustTokenGroups(m_hToken, FALSE, pGroups, cbGroups, NULL, 
		NULL) == TRUE);
}

 /**
 ****************************************************************************
 * Resets groups for the access token to their default enabled and 
 * disabled states. 
 * The CTokenGroups class can be used to manipulate the 
 * TOKEN_GROUPS structure.
 ****************************************************************************
*/
bool CAccessToken::ResetGroups()
{
	if (!m_hToken)
		return false;

	return (::AdjustTokenGroups(m_hToken, TRUE, NULL, 0, NULL, NULL) == TRUE);
}

 /**
 ****************************************************************************
 * Adjusts groups in the access token. 
 * Mandatory groups cannot be disabled. They are identified by the 
 * SE_GROUP_MANDATORY attribute in the TOKEN_PRIVILEGES structure. If an 
 * attempt is made to disable any mandatory groups, AdjustTokenGroups fails 
 * and leaves the state of all groups unchanged.
 *
 * The CTokenGroups class can be used to manipulate the 
 * TOKEN_PRIVILEGES structure.
 *
 * TOKEN_ADJUST_GROUPS access is required to enable or disable groups in an
 * access token.
 ****************************************************************************
 */
bool CAccessToken::AdjustPrivileges(const PTOKEN_PRIVILEGES pPrivs)
{
	if (!pPrivs || !m_hToken)
		return false;

	DWORD cbPrivs = CTokenPrivileges::GetSize(pPrivs);
	return (::AdjustTokenPrivileges(m_hToken, FALSE, pPrivs, cbPrivs, NULL, 
		NULL) == TRUE);
}

 /**
 ****************************************************************************
 * Disable ALL privileges within an access token. 
 *
 * Enabling or disabling privileges in an access token requires 
 * TOKEN_ADJUST_PRIVILEGES access.
 ****************************************************************************
 */
bool CAccessToken::DisableAllPrivileges()
{
	if (!m_hToken)
		return false;

	return (::AdjustTokenPrivileges(m_hToken, TRUE, NULL, 0, NULL, NULL) 
		== TRUE);
}

 /**
 ****************************************************************************
 * Determine whether a specified CSID is enabled in an access token.
 ****************************************************************************
 */
bool CAccessToken::IsSIDEnabled(const PSID pSid)
{
	if (!m_hToken)
		return false;

	CSID sid(GetOwner());
	if (sid == CSID(pSid))
		return true;

	CTokenGroups group(GetGroups());
	if (group.IsSIDEnabled(pSid))
		return true;

	return false;
}

 /**
 ****************************************************************************
 * Enables or disables a privilege (which must exist in the access 
 * token).  
 * Enabling or disabling privileges in an access token requires 
 * TOKEN_ADJUST_PRIVILEGES access.
 ****************************************************************************
 */
bool CAccessToken::SetPrivilege(const PLUID pLuid, bool bEnablePrivilege)
{
	if (!m_hToken)
		return false;

	TOKEN_PRIVILEGES tp;

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = *pLuid;

	if (bEnablePrivilege)
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	else
		tp.Privileges[0].Attributes = 0;

	return AdjustPrivileges(&tp);
}

 /**
 ****************************************************************************
 * Search for a specified privilege within the access token.
 ****************************************************************************
 */
bool CAccessToken::FindPrivilege(
	const PLUID pLuid, 
	LUID_AND_ATTRIBUTES * & pPrivilege)
{
	if (!m_hToken || !pLuid)
		return false;

	TOKEN_PRIVILEGES * pPrivs = (TOKEN_PRIVILEGES *)GetPrivileges();
	if (!pPrivs)
		return false;

	PLUID pLuidCompare;
	for(DWORD dwPriv = 0; dwPriv < pPrivs->PrivilegeCount; dwPriv++)
	{
		// Compare the luids and set pointer to the luid and attributes if 
		// There is a match.
		pLuidCompare = &pPrivs->Privileges[dwPriv].Luid;

		if (pLuidCompare->HighPart == pLuid->HighPart && 
			pLuidCompare->LowPart == pLuid->LowPart)
		{
			pPrivilege = &pPrivs->Privileges[dwPriv];
			return true;
		}
	}

	return false;
}

 /**
 ****************************************************************************
 * Create the access token from a specific user - the access token can 
 * then be passed to ::ImpersonateLoggedOnUser() for impersonation.
 ****************************************************************************
 */
bool CAccessToken::CreateFromUser(
	LPCTSTR lpszDomain, LPCTSTR lpszUser, LPCTSTR lpszPwd)
{	
	HANDLE hToken;
	if (::LogonUser((LPTSTR)lpszUser, (LPTSTR)lpszDomain, (LPTSTR)lpszPwd, 
		LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, &hToken))
	{
		*this = hToken;
		return true;
	}

	return false;
}

std::basic_string<TCHAR> CAccessToken::Dump()
{
	if (!m_hToken)
		return std::basic_string<TCHAR>(_T("ACCESS TOKEN - Null Handle"));

	std::basic_string<TCHAR> strResults(_T("ACCESS TOKEN - "));

	if (IsPrimaryType())
		strResults += _T("Primary Type, ");
	else
	{
		strResults += _T("Impersonation Type(");

		switch(GetImpersonationLevel())
		{
		default: strResults += _T("Unknown), "); break;
		case SecurityAnonymous: strResults += _T("Anonymous), "); break;
		case SecurityDelegation: strResults += _T("Delegation), "); break;
		case SecurityImpersonation: strResults += _T("Impersonation), "); break;
		case SecurityIdentification: strResults += _T("Identification), "); break;
		}
	}

	TCHAR szSize[10];
	::_ltot_s(GetSessionId(), szSize, 10);
	strResults += _T("SessionId: ");
	strResults += szSize;
	strResults += _T(", ");

	strResults += _T("Token Source: \"");
	PTOKEN_SOURCE pSource = GetSource();
	std::basic_string<TCHAR> strSource(_T("Unknown"));
	if (pSource)
	{
		CA2CT szSource(pSource->SourceName);
		strSource = szSource;
	}
	strResults += strSource;
	strResults += _T("\"");

	// Obtain luid from the token source.
	CPrivilege priv(pSource ? &pSource->SourceIdentifier : NULL);
	strResults += priv.Dump();

	CSID sid(GetOwner());
	strResults += _T(", Owner ");
	strResults += sid.Dump();

	sid = GetUser();
	strResults += _T(", User ");
	strResults += sid.Dump();

	sid = GetPrimaryGroup();
	strResults += _T(", Primary Group ");
	strResults += sid.Dump();

	CACL acl;
	acl = GetDefaultDACL();
	strResults += _T(", Default DACL ");
	strResults += acl.Dump();

	CTokenGroups groups(GetGroups());
	strResults += _T(", Groups ");
	strResults += groups.Dump();

	groups = GetRestrictingSids();
	strResults += _T(", Restricting Sids ");
	strResults += groups.Dump();

	CTokenPrivileges privs(GetPrivileges());

	strResults += _T(", Privileges ");
	strResults += privs.Dump();

	return strResults;
}
		 
std::basic_string<TCHAR> CAccessToken::DumpXML()
{
	if (!m_hToken)
		return std::basic_string<TCHAR>(_T("<ACCESS_TOKEN/>"));

	std::basic_string<TCHAR> strResults(_T("<ACCESS_TOKEN TOKEN_TYPE="));
	if (IsPrimaryType())
		strResults += _T("TokenPrimary");
	else
		strResults += _T("TokenImpersonation");

	strResults += _T(" SECURITY_IMPERSONATION_LEVEL=");

	switch(GetImpersonationLevel())
	{
	default: strResults += _T("Unknown"); break;
	case SecurityAnonymous: strResults += _T("SecurityAnonymous"); break;
	case SecurityDelegation: strResults += _T("SecurityDelegation"); break;
	case SecurityImpersonation: strResults += _T("SecurityImpersonation"); break;
	case SecurityIdentification: strResults += _T("SecurityIdentification"); break;
	}

	TCHAR szSize[10];
	::_ltot_s(GetSessionId(), szSize, 10);
	strResults += _T(" SessionId=");
	strResults += szSize;
	strResults += _T(">");

	strResults += _T("<TOKEN_SOURCE Name=");
	PTOKEN_SOURCE pSource = GetSource();
	std::basic_string<TCHAR> strSource(_T("\"\""));
	if (pSource)
	{
		CA2CT szSource(pSource->SourceName);
		strSource = szSource;
	}
	strResults += strSource;
	strResults += _T("><SourceIdentifier>");

	// Obtain luid from the token source
	CPrivilege priv(pSource ? &pSource->SourceIdentifier : NULL);
	strResults += priv.DumpXML();
	strResults += _T("</SourceIdentifier></TOKEN_SOURCE>");

	CSID sid(GetOwner());
	strResults += _T("<OWNER>");
	strResults += sid.DumpXML();
	strResults += _T("</OWNER>");

	sid = GetUser();
	strResults += _T("<USER>");
	strResults += sid.DumpXML();
	strResults += _T("</USER>");

	sid = GetPrimaryGroup();
	strResults += _T("<PRIMARYGROUP>");
	strResults += sid.DumpXML();
	strResults += _T("</PRIMARYGROUP>");

	CACL acl;
	acl = GetDefaultDACL();
	strResults += _T("<DefaultDACL>");
	strResults += acl.DumpXML();
	strResults += _T("</DefaultDACL>");

	CTokenGroups groups(GetGroups());
	strResults += _T("<GROUPS>");
	strResults += groups.DumpXML();
	strResults += _T("</GROUPS>");

	groups = GetRestrictingSids();
	strResults += _T("<RestrictingSids>");
	strResults += groups.DumpXML();
	strResults += _T("</RestrictingSids>");

	CTokenPrivileges privs(GetPrivileges());

	strResults += _T("<Privileges>");
	strResults += privs.DumpXML();
	strResults += _T("</Privileges>");

	strResults += _T("</ACCESS_TOKEN>");
	return strResults;
}
