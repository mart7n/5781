#pragma once

#include <windows.h>
#include <aclapi.h>

namespace NTSecurity
{
	class CLoggedOnSID
	{
	public:
		CLoggedOnSID();
		~CLoggedOnSID();

		PSID GetSID();

	private:
		SID * m_pSID;
	};

	PSID GetLoggedOnSID();
}
