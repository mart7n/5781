#include <assert.h>
#include "SIDs.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// SIDs members.
//////////////////////////////////////////////////////////////////////////////
DWORD SIDs::GetLogOnAccountSID(SID * & pSID)
{
	pSID = NULL;

	PTOKEN_USER pTokenUser;
	DWORD dwResult = SIDs::GetCurrentProcessUserTokens(pTokenUser);

	if (dwResult == ERROR_SUCCESS)
	{
		DWORD dwLength = 0;
		DWORD dwNameSize = 0;
		DWORD dwDomainSize = 0;
		SID_NAME_USE sidNameUser;

		::LookupAccountSid(NULL, pTokenUser->User.Sid, NULL, &dwNameSize, NULL,
			&dwDomainSize, &sidNameUser);

		LPTSTR pszName = new TCHAR[dwNameSize + 1];
		LPTSTR pszDomain = new TCHAR[dwDomainSize + 1];

		if (::LookupAccountSid(NULL, pTokenUser->User.Sid, pszName, &dwNameSize,
			pszDomain, &dwDomainSize, &sidNameUser))
		{
			dwLength = ::GetLengthSid(pTokenUser->User.Sid);
			pSID = (SID*)::HeapAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY,
				dwLength);

			if (!::CopySid(dwLength, pSID, pTokenUser->User.Sid))
			{
				dwResult = ::GetLastError();
				::HeapFree(::GetProcessHeap(), 0, (LPVOID)pSID);
				pSID = NULL;
			}
		}

		delete[] pszName;
		delete[] pszDomain;
		::HeapFree(::GetProcessHeap(), 0, (LPVOID)pTokenUser);
	}

	assert((dwResult == ERROR_SUCCESS) ? (pSID != NULL) : (pSID == NULL));
	return dwResult;
}

DWORD SIDs::GetCurrentProcessUserTokens(PTOKEN_USER & pTokenUser)
{
	pTokenUser = NULL;

	HANDLE hToken;
	DWORD dwLength = 0;
	DWORD dwResult = ERROR_SUCCESS;

	if (::OpenProcessToken(::GetCurrentProcess(), TOKEN_QUERY, &hToken))
	{
		::GetTokenInformation(hToken, TokenUser, NULL, 0, &dwLength);
		pTokenUser = (PTOKEN_USER)::HeapAlloc(::GetProcessHeap(),
			HEAP_ZERO_MEMORY, dwLength);

		if (!::GetTokenInformation(hToken, TokenUser, (LPVOID)pTokenUser,
			dwLength, &dwLength))
		{
			dwResult = ::GetLastError();
			::HeapFree(::GetProcessHeap(), 0, (LPVOID)pTokenUser);
			pTokenUser = NULL;
		}

		::CloseHandle(hToken);
	}
	else
		dwResult = ::GetLastError();

	assert((dwResult == ERROR_SUCCESS) ?
		(pTokenUser != NULL) : (pTokenUser == NULL));
	return dwResult;
}
