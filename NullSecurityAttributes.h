#pragma once

#include <windows.h>
#include <tchar.h>
#include <assert.h>

namespace NTSecurity
{
	class CNullSecurityAttributes
	{
	public:
		CNullSecurityAttributes();

		SECURITY_ATTRIBUTES * operator &();
		const SECURITY_ATTRIBUTES * operator &() const;
		
	private:
		SECURITY_DESCRIPTOR m_sd;
		SECURITY_ATTRIBUTES m_sa;	
	};

	typedef CNullSecurityAttributes CNullSA;
}
