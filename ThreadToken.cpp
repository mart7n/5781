#include <assert.h>
#include "ThreadToken.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CThreadToken members.
//////////////////////////////////////////////////////////////////////////////
CThreadToken::CThreadToken(
	bool bProcessContext,
	DWORD dwAccess,
	HANDLE hThread)
  : m_hToken(NULL)
{
	BOOL bOpenAsSelf = (bProcessContext == FALSE);
	if (!::OpenThreadToken(hThread, dwAccess, bOpenAsSelf, &m_hToken))
		m_hToken = NULL;
}

CThreadToken::~CThreadToken()
{
	if (m_hToken) 
		::CloseHandle(m_hToken);
}

CThreadToken::operator HANDLE()
{
	return GetHANDLE();
}

HANDLE CThreadToken::GetHANDLE()
{
	return m_hToken;
}
