#include <assert.h>
#include "Privilege.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CSID members.
//////////////////////////////////////////////////////////////////////////////
CPrivilege::CPrivilege(const PLUID pLuid) 
{ 
	*this = pLuid; 
}

CPrivilege::CPrivilege(const CPrivilege &other) 
{ 
	*this = (const PLUID)&other.m_luid; 
}

CPrivilege::CPrivilege(
	LPCTSTR pszPrivilegeName, 
	LPCTSTR pszSystem)
{ 
	SetName(pszSystem, pszPrivilegeName); 
}

PLUID CPrivilege::operator=(const PLUID pLuid)
{
	if (!pLuid)
	{
		m_luid.LowPart = 0;
		m_luid.HighPart = 0;
	}
	else if (pLuid != &m_luid)
		m_luid = *pLuid;

	return &m_luid;
}

CPrivilege & CPrivilege::operator=(const CPrivilege &other)
{
	*this = (const PLUID)&other.m_luid;
	return *this;
}

CPrivilege::operator PLUID() { return &m_luid; }    

bool CPrivilege::CreateUnique() 
{
	return (::AllocateLocallyUniqueId(&m_luid) == TRUE); 
}

bool CPrivilege::SetName(LPCTSTR pszSystem, LPCTSTR pszName)
{
	LUID luid;
	if (::LookupPrivilegeValue(pszSystem, pszName, &luid))
	{
		m_luid = luid;
		return true;
	}

	return false;
}

bool CPrivilege::GetName(
	LPCTSTR pszSystem, 
	std::basic_string<TCHAR> &strName) const
{
	DWORD cbName = 0;   
	::LookupPrivilegeName(pszSystem, (PLUID)&m_luid, NULL, &cbName);

	if (cbName)
	{
		if (strName.size() < cbName)
			strName.resize(cbName);

		if (::LookupPrivilegeName(pszSystem, (PLUID)&m_luid, 
			(LPTSTR)strName.data(), &cbName))
		{
			strName.resize(cbName, 1);
			return true;
		}
	}

	return false;
}

std::basic_string<TCHAR> CPrivilege::Dump() const
{
	std::basic_string<TCHAR> strResults = _T("Luid - LowPart: 0x0");

	TCHAR szSize[10];

	::_ltot_s(m_luid.LowPart, szSize, 16);
	strResults += szSize;
	strResults += _T(", HighPart: 0x0");
	::_ltot_s(m_luid.HighPart, szSize, 16);
	strResults += szSize;

	std::basic_string<TCHAR> strName;

	if (GetName(NULL, strName))
	{
		strResults += _T(" (");
		strResults += strName;
		strResults += _T(")");
	}

	return strResults;
}

std::basic_string<TCHAR> CPrivilege::DumpXML() const
{
	std::basic_string<TCHAR> strResults = _T("<LUID LowPart=0x0");

	TCHAR szSize[10];

	::_ltot_s(m_luid.LowPart, szSize, 16);
	strResults += szSize;
	strResults += _T(" HighPart=0x0");
	::_ltot_s(m_luid.HighPart, szSize, 16);
	strResults += szSize;

	strResults += _T(" Name=");
	std::basic_string<TCHAR> strName;

	if (GetName(NULL, strName))
		strResults += strName;     
	else
		strResults += _T("\"\"");     

	strResults += _T(">");     
	return strResults;
}
