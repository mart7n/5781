#pragma once

#include <windows.h>
#include <tchar.h>
#include <string>
#include <aclapi.h>
#include "Privilege.h"

namespace NTSecurity
{	
	class CTokenPrivileges
	{
	public:
		CTokenPrivileges(const PTOKEN_PRIVILEGES = NULL);
		~CTokenPrivileges();

		PTOKEN_PRIVILEGES operator=(const PTOKEN_PRIVILEGES);

		operator PTOKEN_PRIVILEGES();
		operator const PTOKEN_PRIVILEGES() const;

		DWORD GetPrivilegeCount() const;
		PLUID GetPrivilege(DWORD, DWORD &) const;
		static DWORD GetSize(const PTOKEN_PRIVILEGES);

		bool DeletePrivilege(DWORD);
		bool AppendPrivilege(const PLUID, DWORD);
		bool InsertPrivilege(const PLUID, DWORD, DWORD);

		std::basic_string<TCHAR> Dump() const;
		std::basic_string<TCHAR> DumpXML() const;

	private:
		void Free();
		
		CPrivilege		  * m_pPrivs;
		PTOKEN_PRIVILEGES   m_pTokPrivs;
	};
}
