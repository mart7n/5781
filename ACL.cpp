#include <assert.h>
#include "ACL.h"
#include "SID.h"
#include "HeapAllocate.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CACL members.
//////////////////////////////////////////////////////////////////////////////
CACL::CACL(const PACL pAcl) : m_pAcl(NULL)
{
	*this = pAcl;
}

CACL::CACL(const CACL & other) : m_pAcl(NULL) 
{ 
	*this = other.m_pAcl; 
}

CACL::~CACL() 
{ 
	Free(); 
}

void CACL::Free()
{
	CHeapAllocate::Free(m_pAcl);
}

CACL & CACL::operator=(const CACL &other) 
{ 
	*this = other.m_pAcl; 
	return *this; 
}

PACL CACL::operator=(const PACL pAcl)
{
	if ((LPBYTE)pAcl != (LPBYTE)m_pAcl)
	{
		Free();

		if (pAcl)
		{
			ACL_SIZE_INFORMATION AclSize;

			if (::GetAclInformation(
				pAcl,							// access-control list
				&AclSize,						// ACL information
				sizeof(ACL_SIZE_INFORMATION),	// size of ACL information
				AclSizeInformation))			// info class
			{		
				DWORD cbNewAcl = AclSize.AclBytesInUse;
				CHeapAllocate allocator(cbNewAcl);
				m_pAcl = (PACL)allocator.Detach();

				::InitializeAcl(m_pAcl, cbNewAcl, ACL_REVISION);

				LPVOID lpAce;						
				for (int nAce = 0; nAce < pAcl->AceCount; nAce++)
				{
					if (::GetAce(pAcl, nAce,(void**) &lpAce))
					{
						::AddAce(m_pAcl, ACL_REVISION, MAXDWORD, lpAce, 
							((ACE_HEADER*)lpAce)->AceSize);
					}
				}	
			}				
		}
	}

	return m_pAcl;
}

CACL::operator PACL() 
{ 
	return m_pAcl; 
}

CACL::operator const PACL() const 
{ 
	return m_pAcl; 
}

BYTE CACL::GetRevision() const 
{ 
	return m_pAcl ? m_pAcl->AclRevision : 0; 
}

DWORD CACL::GetSize() const 
{ 
	return m_pAcl ? m_pAcl->AclSize : 0; 
}

DWORD CACL::GetACECount() const 
{ 
	return m_pAcl ? m_pAcl->AceCount : 0; 
}

bool CACL::AppendACE(const PACE lpAce) 
{ 
	return InsertACE(lpAce, MAXDWORD); 
}

PACE CACL::GetACE(DWORD nIndex) const
{
	LPVOID lpAce = NULL;
	if (::GetAce((PACL)m_pAcl, nIndex, &lpAce))
		return (PACE)lpAce;

	return NULL;
}

bool CACL::InsertACE(const PACE lpAce, DWORD dwIndex)
{
	DWORD dwSizeACE = CACE::GetSizeACE(lpAce);
	if (dwSizeACE == 0)
		return false;

	return InsertACE(lpAce, dwIndex, dwSizeACE);
}

/**
****************************************************************************
* Given an access control list and a size to extend, this function 
* allocates room for the old access control list + number of bytes specified 
* in the size to extend then attempts to copy access control entries from 
* the old access control list into the new one.  The total number of access
* control entries added to the new access control list is the return value.
* The new access control list will be NULL if the old access control list 
* pointer was NULL.
****************************************************************************
*/
int CACL::ExtendAndCopyACL(
	const PACL lpOldAcl, 
	DWORD dwSizeExtend, 
	PACL & lpNewAcl)
{
	lpNewAcl = NULL;

	if (!lpOldAcl)
		return 0;

	ACL_SIZE_INFORMATION AclSize;

	if (::GetAclInformation(
		lpOldAcl,						// access-control list
		&AclSize,						// ACL information
		sizeof(ACL_SIZE_INFORMATION),	// size of ACL information
		AclSizeInformation))			// info class
	{		
		DWORD cbNewAcl = AclSize.AclBytesInUse + dwSizeExtend;
		CHeapAllocate allocator(cbNewAcl);
		lpNewAcl = (PACL)allocator.Detach();

		::InitializeAcl(lpNewAcl, cbNewAcl, ACL_REVISION);

		LPVOID lpAce;		
		int nAddedAces = 0;		
		for (int nAce = 0; nAce < lpOldAcl->AceCount; nAce++)
		{
			// Obtain old ace and add to new list			
			if (::GetAce(lpOldAcl, nAce,(void**) &lpAce))
			{
				if (::AddAce(lpNewAcl, ACL_REVISION, MAXDWORD, lpAce, 
					((ACE_HEADER*)lpAce)->AceSize))
				{
					nAddedAces++;
				}
			}
		}	

		return nAddedAces;
	}

	return 0;
}

 int CACL::ExtendACL(DWORD dwSizeExtend)
{
	PACL lpNewAcl;
	int nAces = ExtendAndCopyACL(m_pAcl, dwSizeExtend, lpNewAcl);

	if (lpNewAcl)
	{
		if ((GetACECount() == nAces))
		{			
			Free();
			m_pAcl = lpNewAcl;
			return true;
		}

		CHeapAllocate::Free(lpNewAcl); 
	}

	return false;
}

 bool CACL::InsertACE(const PACE lpAce, DWORD dwIndex, DWORD dwSizeACE)
{	
	if (!m_pAcl)
		CreateEmptyACL();

	PACL lpNewAcl;
	int nAces = ExtendAndCopyACL(m_pAcl, dwSizeACE, lpNewAcl);

	if (lpNewAcl)
	{
		if ((GetACECount() == nAces))
		{
			if (dwIndex > (DWORD)nAces) 				
				dwIndex = MAXDWORD;

			if (::AddAce(lpNewAcl, ACL_REVISION, dwIndex, lpAce, dwSizeACE))
			{
				Free();
				m_pAcl = lpNewAcl;
				return true;
			}
		}

		CHeapAllocate::Free(lpNewAcl); 
	}

	return false;
}

void CACL::CreateEmptyACL()
{
	Free();

	DWORD cbAcl = sizeof(ACL);
	CHeapAllocate allocator(cbAcl);
	m_pAcl = (PACL)allocator.Detach();

	::InitializeAcl(m_pAcl, cbAcl, ACL_REVISION);
}

 /**
 ****************************************************************************
 * Validates an ACL.  Calls Win32 ::IsValidAcl(), obtains each ace from 
 * the acl, then validates the sid belonging to the ace by attempting to 
 * obtain the name of the account based on the sid.  Returns false if any 
 * part of the ACL is invalid.  If a sid or ace is in error then a zero based 
 * index representing the ace is returned in dwBadACE.  If the acl is in 
 * error for any other reason (not related to an ace or sid) then dwBadACE 
 * will be -1.
 ****************************************************************************
 */
bool CACL::IsValid(DWORD & dwBadACE) const 
{
	dwBadACE = -1;

	if (!m_pAcl || !::IsValidAcl(m_pAcl))
		return false;

	PACE pAce;
	DWORD dwAceCount = GetACECount();	
	for (DWORD dwAce=0; dwAce<dwAceCount; dwAce++)
	{
		pAce = GetACE(dwAce);

		if (!pAce)
		{
			dwBadACE = dwAce;
			return false;
		}

		CACE ace(pAce);
		if (!ace.IsValid())
		{
			dwBadACE = dwAce;
			return false;
		}
	}

	return true;
}

DWORD CACL::RemoveInvalidACES()
{
	DWORD dwTotalBad = 0;
	DWORD dwBadACE;

	while(!IsValid(dwBadACE))
	{
		if (DeleteACE(dwBadACE))
			dwTotalBad++;
	}

	return dwTotalBad;
}

 /**
 ****************************************************************************
 * Re-orders the ACL so that access denied access control entries precede
 * access allowed entries.  Returns true if the order was changed.
 ****************************************************************************
 */
bool CACL::ReOrder()
{
	if (!m_pAcl)
		return false;

	DWORD dwAceCount = GetACECount();
	if (dwAceCount < 2)
		return false;

	CACL aclDenied;
	CACL aclAllowed;

	PACE pAce;
	bool bIsBadOrder = false;
	bool bHasAllowedEntries = false;
	
	for(DWORD dwAce=0; dwAce<dwAceCount; dwAce++)
	{
		pAce = GetACE(dwAce);
		if (!pAce)
			return false;

		// The access control entry should not be an audit type
		CACE ace(pAce);
		if (ace.IsSystemAudit())
			return false;

		// Add ace to the appropriate list		
		if (ace.IsAllowedAccess())
		{
			bHasAllowedEntries = true;
			if (!aclAllowed.AppendACE((PACE)ace))
				return false;
		}
		else
		{
			// If a allowed entry has been encountered (followed by an denied
			// entry) then the order is bad.			
			if (bHasAllowedEntries)
				bIsBadOrder = true;

			if (!aclDenied.AppendACE((PACE)ace))
				return false;
		}
	}

	if (bIsBadOrder)
	{				
		// Add entries from the allowed access control list to denied list
		dwAceCount = aclAllowed.GetACECount();
		for(DWORD dwAce = 0; dwAce < dwAceCount; dwAce++)
		{
			pAce = aclAllowed.GetACE(dwAce);
			if (!pAce || !aclDenied.AppendACE(pAce))
				return false;
		}

		// Set contents of this acl to the denied access control list which 
		// now has the allowed access control entries added to the end.
		*this = (PACL)aclDenied;

		return true;
	}

	return false;
}

 bool CACL::DeleteACE(DWORD dwIndex)
{
	if (!m_pAcl)
		return false;

	return ::DeleteAce(m_pAcl, dwIndex) == TRUE;
}

std::basic_string<TCHAR> CACL::Dump() const
{
	if (!m_pAcl)
		return std::basic_string<TCHAR>(_T("ACL - Unallocated"));

	TCHAR szSize[10];

	std::basic_string<TCHAR> strResults(_T("ACL - Size: "));
	::_ltot_s(GetSize(), szSize, 10);
	strResults += szSize;

	strResults += _T(", Aces: ");
	::_ltot_s(GetACECount(), szSize, 10);
	strResults += szSize;

	strResults += _T(", Revision: ");
	::_ltot_s(GetRevision(), szSize, 10);
	strResults += szSize;

	strResults += _T(", ");

	DWORD dwAceCount = GetACECount();
	for (DWORD dwAce = 0; dwAce < dwAceCount; dwAce++)
	{
		PACE pAce = GetACE(dwAce);

		if (pAce)
		{
			CACE ace(pAce);
			strResults += ace.Dump();
			if (dwAce + 1 != dwAceCount)
			{
				strResults += _T(" ");
			}
		}
	}

	return strResults;
}

std::basic_string<TCHAR> CACL::DumpXML() const
{
	if (!m_pAcl)
		return std::basic_string<TCHAR>(_T("<ACL/>"));

	TCHAR szSize[10];

	std::basic_string<TCHAR> strResults(_T("<ACL Size="));
	::_ltot_s(GetSize(), szSize, 10);
	strResults += szSize;

	strResults += _T(" Revision=");
	::_ltot_s(GetRevision(), szSize, 10);
	strResults += szSize;

	strResults += _T(" Aces=");
	::_ltot_s(GetACECount(), szSize, 10);
	strResults += szSize;

	strResults += _T(">");

	DWORD dwAceCount = GetACECount();
	for (DWORD dwAce = 0; dwAce < dwAceCount; dwAce++)
	{
		PACE pAce = GetACE(dwAce);
		if (pAce)
		{
			CACE ace(pAce);
			strResults += ace.DumpXML();
		}
	}

	strResults += _T("</ACL>");
	return strResults;
}
