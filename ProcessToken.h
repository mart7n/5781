#pragma once

#include <windows.h>
#include <tchar.h>
#include <aclapi.h>

namespace NTSecurity
{
	class CProcessToken
	{
	public:
		CProcessToken(
			DWORD = TOKEN_QUERY,
			HANDLE = ::GetCurrentProcess());

		~CProcessToken();

		operator HANDLE();
		HANDLE GetHANDLE();

	private:
		HANDLE m_hToken;
	};
}