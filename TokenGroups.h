#pragma once

#include <windows.h>
#include <tchar.h>
#include <string>
#include <aclapi.h>
#include "SID.h"

namespace NTSecurity
{
	class CTokenGroups
	{
	public:
		CTokenGroups(const PTOKEN_GROUPS = NULL);
		~CTokenGroups();
		PTOKEN_GROUPS operator=(const PTOKEN_GROUPS);

		operator PTOKEN_GROUPS();
		operator const PTOKEN_GROUPS() const;

		DWORD GetSize() const;
		DWORD GetSIDCount() const;
		PSID GetSID(DWORD, DWORD &) const;
		static DWORD GetSize(PTOKEN_GROUPS);

		/*
		*********************************************************************
		* Obtain the logged on sid (See "mk:@MSITStore:d:\MSDN98\98VSa\1033\
		* winbase.chm::/devdoc/live/pdwbase/accclsrv_1pk4.htm" in the MSDN).
		*********************************************************************
		*/
		PSID GetLogonSID(DWORD &, DWORD &) const;

		bool DeleteSID(DWORD);
		bool AppendSID(const PSID, DWORD);
		bool InsertSID(const PSID, DWORD, DWORD);

		/*
		*********************************************************************
		* Search for specified sid within the group.  This search starts at
		* the position based on a zero based index specified by the dwIndex
		* parameter (for example call FindSID(pSid, 0) to start the search
		* from start of the list.)
		* If the search is successful the index of the sid will be returned
		* in dwIndex.  This can then be used in a call the
		* CTokenGroups::GetSID().
		*********************************************************************
		*/
		bool FindSID(const PSID, DWORD &) const;
		bool IsSIDEnabled(const PSID) const;

		std::basic_string<TCHAR> Dump() const;
		std::basic_string<TCHAR> DumpXML() const;

	private:
		void Free();
		
		CSID		 * m_pSids;
		TOKEN_GROUPS * m_pGroups;
	};
}