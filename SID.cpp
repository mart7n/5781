#include <assert.h>
#include "SID.h"
#include "Strtok.h"
#include "HeapAllocate.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CSID members.
//////////////////////////////////////////////////////////////////////////////
CSID::CSID(const PSID pSid) : m_pSid(NULL)
{
	*this = pSid;
}

CSID::CSID(const CSID &other) : m_pSid(NULL) 
{ 
	*this = other.m_pSid; 
}

CSID::CSID(LPCTSTR pszAccount, LPCTSTR pszSystem) : m_pSid(NULL)
{
	SetAccount(pszSystem, pszAccount);
}

CSID::~CSID() 
{ 
	Free(); 
}

void CSID::Free()
{
	CHeapAllocate::Free(m_pSid);	
}

CSID & CSID::operator=(const CSID &other) 
{ 
	*this = other.m_pSid; 
	return *this; 
}

bool CSID::operator==(const PSID pSid)
{
	return ::EqualSid(m_pSid, (PSID)pSid) == TRUE;
}

CSID::operator PSID() 
{ 
	return m_pSid; 
}

CSID::operator const PSID() const 
{ 
	return m_pSid; 
}

 /**
 ****************************************************************************
 * Validates a sid on the local system.
 ****************************************************************************
 */
bool CSID::IsValid() const
{
	return IsValid(NULL);
}

PSID CSID::operator=(const PSID pSid)
{
	if ((LPBYTE)pSid != (LPBYTE)m_pSid)
	{
		Free();

		if (pSid)
		{			
			DWORD cbSid = ::GetLengthSid((PSID)pSid);
			CHeapAllocate allocator(cbSid);
			m_pSid = (PSID)allocator.Detach();

			::CopySid(cbSid, m_pSid, (PSID)pSid);
		}
	}

	return m_pSid;
}

 /**
 ****************************************************************************
 * Obtain the sid in string format.
 ****************************************************************************
 */
bool CSID::GetString(std::basic_string<TCHAR> & strSid) const
{
	PSID_IDENTIFIER_AUTHORITY psia;    
	DWORD dwSubAuthorities;
	DWORD dwSidRev=SID_REVISION;    
	DWORD dwCounter;
	DWORD dwSidSize;

	if (!m_pSid || !IsValidSid(m_pSid))
		return false;

	psia = ::GetSidIdentifierAuthority(m_pSid);
	
	// Compute the buffer length.
	// S-SID_REVISION- + IdentifierAuthority- + subauthorities- + NULL
	dwSubAuthorities = *::GetSidSubAuthorityCount(m_pSid);
	dwSidSize=(15 + 12 + (12 * dwSubAuthorities) + 1) * sizeof(TCHAR);

	std::basic_string<TCHAR> strTempSid;
	strTempSid.resize(dwSidSize);
	LPTSTR pszSid = (LPTSTR)strTempSid.data();

	// Add 'S' prefix and revision number to the string.
	size_t cbSid = strTempSid.size();
	dwSidSize= ::_stprintf_s(pszSid, cbSid, _T("S-%lu-"), dwSidRev);

	// Add SID identifier authority to the string.
	if ((psia->Value[0] != 0) || (psia->Value[1] != 0))
	{
		dwSidSize += ::wsprintf(pszSid + lstrlen(pszSid),
			TEXT("0x%02%02%02%02%02%02"),
			(USHORT)psia->Value[0],
			(USHORT)psia->Value[1],
			(USHORT)psia->Value[2],
			(USHORT)psia->Value[3],
			(USHORT)psia->Value[4],
			(USHORT)psia->Value[5]);    
	}    
	else    
	{
		dwSidSize += ::wsprintf(pszSid + lstrlen(pszSid),
			TEXT("%lu"),
			(ULONG)(psia->Value[5]      )   +
			(ULONG)(psia->Value[4] <<  8)   +
			(ULONG)(psia->Value[3] << 16)   +
			(ULONG)(psia->Value[2] << 24)   );    
	}
	
	// Add SID subauthorities to the string.	
	for (dwCounter=0 ; dwCounter < dwSubAuthorities ; dwCounter++)	  
	{
		dwSidSize += ::wsprintf(pszSid + dwSidSize, TEXT("-%lu"),
			*::GetSidSubAuthority(m_pSid, dwCounter));        
	}

	if (strSid.size() <= dwSidSize)
		strSid.resize(dwSidSize);

	strSid.erase();
	strSid.append(strTempSid.c_str(), dwSidSize);

	return true;
}

 /**
 ****************************************************************************
 * Set the sid from string format.
 * This function sets a sid from the string format.
 * The strings format should be:
 *   S-R-I-S-S...
 *
 * In this notation, the first literal character S identifies the series
 * of digits as a SID, R is the revision level, I is the 
 * identifier-authority value, and S... is one or more subauthority 
 * values. 
 *
 * The following example uses this notation to display the well-known 
 * domain-relative SID of the local Administrators group:
 *
 * S-1-5-32-544
 * 
 * StrTok is used to find tokens within the string
 ****************************************************************************
 */
bool CSID::SetString(LPCTSTR pszSidString)
{
	if (!pszSidString)
	{
		return false;
	}

	CStrTok strToken(_T("-"), pszSidString);

	// Bypass the first 'S'
	if (!strToken.GetNext())
		return false;

	LPCTSTR pszNext = strToken.GetNext();
	if (!pszNext)
		return false;

	// DWORD dwRevision = atol(pszNext);
	pszNext = strToken.GetNext();

	if (!pszNext)
		return false;

	DWORD dwIdent = ::_ttol(pszNext);

	DWORD dwSubAuthorities[20];
	DWORD dwMaxAuthorities = 0;

	while(pszNext = strToken.GetNext())
		dwSubAuthorities[dwMaxAuthorities++] = ::_ttol(pszNext);

	if (!dwMaxAuthorities)
		return false;

	SID_IDENTIFIER_AUTHORITY auth = {0, 0, 0, 0, 0, 0};

	auth.Value[5] = (BYTE)dwIdent;
	auth.Value[4] = (BYTE)(dwIdent >> 8);
	auth.Value[3] = (BYTE)(dwIdent >> 16);
	auth.Value[2] = (BYTE)(dwIdent >> 24);  

#pragma warning(disable: 4244)
	CSID::CreateSIDFromList(auth, dwMaxAuthorities, (char*)dwSubAuthorities);
#pragma warning(default: 4244)

	return true;
}

 /**
 ****************************************************************************
 * Allocate a SID (S-1-5) based on NT authority 
 * (SECURITY_NT_SID_AUTHORITY).
 *
 * The SECURITY_NT_AUTHORITY (S-1-5) predefined identifier authority produces 
 * SIDs that are not universal but are meaningful only on Windows NT/Windows 
 * 2000 installations. The following RID values can be used with 
 * SECURITY_NT_AUTHORITY to create well-known SIDs. 
 *
 * Constant                             Identifies 
 ****************************************************************************
 * SECURITY_DIALUP_RID                  Users who log on to terminals using a 
 * (S-1-5-1)                            dial-up modem. This is a group 
 *                                      identifier. 
 * SECURITY_NETWORK_RID                 Users who can log on across a network. 
 * (S-1-5-2)                            This is a group identifier. 
 * SECURITY_BATCH_RID                   Users who can log on using a batch 
 * (S-1-5-3)                            queue facility. This is a group 
 *                                      identifier. 
 * SECURITY_INTERACTIVE_RID             Users who can log on for interactive 
 * (S-1-5-4)                            operation. This is a group identifier. 
 * SECURITY_LOGON_IDS_RID               A logon session. This is used to 
 * (S-1-5-5-X-Y)                        ensure that only processes in a given 
 *                                      logon session can gain access to the 
 *                                      window-station objects for that 
 *                                      session. The X and Y values for these 
 *                                      SIDs are different for each logon 
 *                                      session. The value 
 *                                      SECURITY_LOGON_IDS_RID_COUNT is the 
 *                                      number of RIDs in this identifier 
 *                                      (5-X-Y). 
 * SECURITY_SERVICE_RID                 Accounts authorized to log on as a 
 * (S-1-5-6)                            service. 
 * SECURITY_ANONYMOUS_LOGON_RID         Anonymous logon, or null session 
 * (S-1-5-7)							logon. 
 * SECURITY_PROXY_RID
 * (S-1-5-8)   
 * SECURITY_ENTERPRISE_CONTROLLERS_RID
 * (S-1-5-9) 
 * SECURITY_PRINCIPAL_SELF_RID          The PRINCIPAL_SELF security 
 * (S-1-5-10)                           identifier can be used in the ACL of 
 *                                      a user or group object. During an 
 *                                      access check, the system replaces the 
 *                                      SID with the SID of the object. The 
 *                                      PRINCIPAL_SELF SID is useful for 
 *                                      specifying an inheritable ACE that 
 *                                      applies to the user or group object 
 *                                      that inherits the ACE. It the only 
 *                                      way of representing the SID of a 
 *                                      created object in the default 
 *                                      security descriptor in the schema.  
 * SECURITY_AUTHENTICATED_USER_RID      The authenticated users. 
 * (S-1-5-11) 
 * SECURITY_RESTRICTED_CODE_RID         Restricted code. 
 * (S-1-5-12) 
 * SECURITY_TERMINAL_SERVER_RID         Terminal Services: Automatically 
 * (S-1-5-13)							added to the security token of a user
 *                                      who logs on to a Terminal Server.  
 * SECURITY_LOCAL_SYSTEM_RID            A special account used by the 
 * (S-1-5-18)                           operating system. 
 * SECURITY_NT_NON_UNIQUE
 * (S-1-5-21)   
 * SECURITY_BUILTIN_DOMAIN_RID          The built-in system domain. 
 * (S-1-5-32) 
 ****************************************************************************
 */
void CSID::CreateNTSID(BYTE nSubAuthorityCount, ...)
{
	SID_IDENTIFIER_AUTHORITY auth = SECURITY_NT_AUTHORITY;

	va_list marker;
	va_start(marker, nSubAuthorityCount);
	CreateSIDFromList(auth, nSubAuthorityCount, marker);
	va_end(marker);
}

 /**
 ****************************************************************************
 * Allocate a SID (S-1-0) based on null authority 
 * (SECURITY_NULL_SID_AUTHORITY).  If 0 is passed for the sub authority 
 * count then a NULL sid will be created (S-1-0-0).  This is a security 
 * identifier which represents a group with no members. This is often used 
 * when a SID value is not known.
 ****************************************************************************
 */
void CSID::CreateNullSID(BYTE nSubAuthorityCount, ...)
{
	SID_IDENTIFIER_AUTHORITY auth = SECURITY_NULL_SID_AUTHORITY;

	if (nSubAuthorityCount == 0)
		CreateSID(auth, 1, SECURITY_NULL_RID);
	else
	{
		va_list marker;
		va_start(marker, nSubAuthorityCount);
		CreateSIDFromList(auth, nSubAuthorityCount, marker);
		va_end(marker);
	}
}

 /**
 ****************************************************************************
 * Allocate a SID (S-1-1) based on WORLD authority 
 * (SECURITY_WORLD_SID_AUTHORITY).  If 0 is passed for the sub authority 
 * count then a world sid will be created (S-1-1-0).  This is a 
 * security identifier that includes all users (everyone group). 
 ****************************************************************************
 */
void CSID::CreateWorldSID(BYTE nSubAuthorityCount, ...)
{
	SID_IDENTIFIER_AUTHORITY auth = SECURITY_WORLD_SID_AUTHORITY;

	if (nSubAuthorityCount == 0)
		CreateSID(auth, 1, SECURITY_WORLD_RID);
	else
	{
		va_list marker;
		va_start(marker, nSubAuthorityCount);
		CreateSIDFromList(auth, nSubAuthorityCount, marker);
		va_end(marker);
	}
}

 /**
 ****************************************************************************
 * Allocate a SID (S-1-2) based on local authority 
 * (SECURITY_LOCAL_SID_AUTHORITY).  If 0 is passed for the sub authority 
 * count then a local sid will be created (S-1-2-0).  This is a 
 * security identifier which represents users who log on to terminals locally 
 * (physically) connected to the system.
 ****************************************************************************
 */
void CSID::CreateLocalSID(BYTE nSubAuthorityCount, ...)
{
	SID_IDENTIFIER_AUTHORITY auth = SECURITY_LOCAL_SID_AUTHORITY;
	if (nSubAuthorityCount == 0)
		CreateSID(auth, 1, SECURITY_LOCAL_RID);
	else
	{
		va_list marker;
		va_start(marker, nSubAuthorityCount);
		CreateSIDFromList(auth, nSubAuthorityCount, marker);
		va_end(marker);
	}
}

 /**
 ****************************************************************************
 * Allocate a SID (S-1-3) based on creator authority 
 * (SECURITY_CREATOR_SID_AUTHORITY).  If 0 is passed for the sub authority 
 * count then a creator owner sid will be created (S-1-3-0).  This is a 
 * security identifier to be replaced by the security identifier of the user 
 * who created a new object. This SID is used in inheritable ACEs.
 ****************************************************************************
 */
void CSID::CreateCreatorSID(BYTE nSubAuthorityCount, ...)
{
	SID_IDENTIFIER_AUTHORITY auth = SECURITY_CREATOR_SID_AUTHORITY;

	if (nSubAuthorityCount == 0)
		CreateSID(auth, 1, SECURITY_CREATOR_OWNER_RID);
	else
	{
		va_list marker;
		va_start(marker, nSubAuthorityCount);
		CreateSIDFromList(auth, nSubAuthorityCount, marker);
		va_end(marker);
	}
}

 /**
 ****************************************************************************
 * Allocate any type of sid.
 ****************************************************************************
 */
void CSID::CreateSID(
	const SID_IDENTIFIER_AUTHORITY &auth, 
	BYTE nSubAuthorityCount, 
	...)
{
	va_list marker;
	va_start(marker, nSubAuthorityCount);    
	CreateSIDFromList(auth, nSubAuthorityCount, marker);
	va_end(marker);
}

 /**
 ****************************************************************************
 * Allocate a SID
 ****************************************************************************
 */
void CSID::CreateSIDFromList(
	const SID_IDENTIFIER_AUTHORITY & authority, 
	BYTE nSubAuthorityCount, 
	va_list marker)
{	
	Free();

	DWORD cbSid = ::GetSidLengthRequired(nSubAuthorityCount);
	CHeapAllocate allocator(cbSid);
	m_pSid = (PSID)allocator.Detach();

	::InitializeSid(
		m_pSid, 
		(PSID_IDENTIFIER_AUTHORITY)&authority, 
		nSubAuthorityCount);

	// Obtain sub authorities from optional arguments	
	for(BYTE nCount =0; nCount < nSubAuthorityCount; nCount++)	 
	{        
		DWORD dw = va_arg(marker, DWORD);				
		(*::GetSidSubAuthority(m_pSid, nCount)) = dw;
	}
}

 /**
 ****************************************************************************
 * Obtains details about the type of sid.
 ****************************************************************************
 */
bool CSID::GetSidUse(LPCTSTR pszSystem, SID_NAME_USE &sidUse) const
{
	if (!m_pSid)
		return false;

	DWORD cbName = 0, cbDomain = 0;
	::LookupAccountSid(pszSystem, m_pSid, NULL, &cbName, NULL, &cbDomain, 
		&sidUse);

	std::basic_string<TCHAR> strDomain, strName;
	strDomain.resize(cbDomain+1);
	strName.resize(cbName+1);

	return ::LookupAccountSid(
		pszSystem, 
		m_pSid, 
		(LPTSTR)strName.data(),
		&cbName, 
		(LPTSTR)strDomain.data(), 
		&cbDomain, 
		&sidUse) == TRUE;
}

 /**
 ****************************************************************************
 * Set the account to be associated with the sid
 ****************************************************************************
 */
bool CSID::SetAccount(LPCTSTR pszSystem, 
	LPCTSTR pszAccount)
{
	Free();	

	DWORD cbSid = 0;
	DWORD cbDomain = 0;
	SID_NAME_USE sidUse;

	::LookupAccountName(
		pszSystem,				// system name
		pszAccount,				// account name
		NULL,					// security identifier (NULL to calculate 
								// size)
		&cbSid,					// size fo security identifier
		NULL,					// domain name (NULL to calculate size)
		&cbDomain,				// size of domain name
		&sidUse);				// type of size indicator

	std::basic_string<TCHAR> strDomain;
	if (cbDomain)
		strDomain.resize(cbDomain+1);

	if (cbSid)
	{
		CHeapAllocate allocator(cbSid);
		m_pSid = (PSID)allocator.Detach();
	}

	bool bResult = false;

	if (::LookupAccountName(
		pszSystem,				// system name
		pszAccount,				// account name
		m_pSid,					// security identifier
		&cbSid,					// size fo security identifier
		(LPTSTR)strDomain.data(),// domain name
		&cbDomain,				// size of domain name
		&sidUse))				// type of size indicator
	{	
		bResult = true;	
	}
	else 
		Free();

	return bResult;
}

 /**
 ****************************************************************************
 * Obtain the account associated with the sid. 
 ****************************************************************************
 */
bool CSID::GetAccount(
	LPCTSTR pszSystem, 
	std::basic_string<TCHAR> &strName, 
	std::basic_string<TCHAR> &strDomain) const
{
	if (!m_pSid)
		return false;

	SID_NAME_USE sidUse;
	DWORD cbName = 0, cbDomain = 0;

	::LookupAccountSid(pszSystem, m_pSid, NULL, &cbName, NULL, &cbDomain, 
		&sidUse);

	if (cbDomain && strDomain.size() < cbDomain+1) 
		strDomain.resize(cbDomain+1);

	if (cbName && strName.size() < cbName+1) 
		strName.resize(cbName+1);

	return ::LookupAccountSid(
		pszSystem, 
		m_pSid, 
		(LPTSTR)strName.data(),
		&cbName, 
		(LPTSTR)strDomain.data(), 
		&cbDomain, 
		&sidUse) == TRUE;
}

 /**
 ****************************************************************************
 * Tests whether a SID is valid by first calling ::IsValidSid() then 
 * attempting to obtain the account information for the sid. 
 ****************************************************************************
 */
bool CSID::IsValid(LPCTSTR pszSystem) const
{	
	if (!m_pSid || !::IsValidSid(m_pSid))
		return false;

	std::basic_string<TCHAR> strName;
	std::basic_string<TCHAR> strDomain;
	return GetAccount(pszSystem, strName, strDomain);
}

std::basic_string<TCHAR> CSID::Dump() const
{
	if (!m_pSid)
		return std::basic_string<TCHAR>(_T("CSID - Unallocated"));

	SID_NAME_USE sidUse;

	std::basic_string<TCHAR> strResults = _T("CSID");
	std::basic_string<TCHAR> strAccount, strDomain, strSid;

	if (GetString(strSid))
	{
		strResults += _T(" (");
		strResults += strSid;
		strResults += _T(")");
	}

	if (GetAccount(NULL, strAccount, strDomain))
	{		
		GetSidUse(NULL, sidUse);

		strResults += _T(" - Account: ");
		strResults += strAccount.c_str();
		strResults += _T(", Domain: ");
		strResults += strDomain.c_str();
		strResults += _T(", Usage: ");

		switch(sidUse)
		{
		default: strResults += _T("Undefined"); break;
		case SidTypeUser: strResults += _T("SidTypeUser"); break;
		case SidTypeGroup: strResults += _T("SidTypeGroup"); break;
		case SidTypeAlias: strResults += _T("SidTypeAlias"); break;		
		case SidTypeDomain: strResults += _T("SidTypeDomain"); break;
		case SidTypeInvalid: strResults += _T("SidTypeInvalid"); break;
		case SidTypeUnknown: strResults += _T("SidTypeUnknown"); break;
		case SidTypeComputer: strResults += _T("SidTypeComputer"); break;
		case SidTypeWellKnownGroup: strResults += _T("SidTypeWellKnownGroup"); break;
		case SidTypeDeletedAccount: strResults += _T("SidTypeDeletedAccount"); break;}														
	}
	else 
		strResults += _T(" - unable to obtain account details");

	return strResults;
}

std::basic_string<TCHAR> CSID::DumpXML() const
{
	if (!m_pSid)
		return std::basic_string<TCHAR>(_T("<CSID/>"));

	SID_NAME_USE sidUse;
	std::basic_string<TCHAR> strResults = _T("<CSID Name=");
	std::basic_string<TCHAR> strAccount, strDomain, strSid;

	if (GetString(strSid))
		strResults += strSid;     
	else
		strResults += _T("\"\"");

	if (GetAccount(NULL, strAccount, strDomain))
	{		
		GetSidUse(NULL, sidUse);

		strResults += _T(" Account=\"");
		strResults += strAccount.c_str();
		strResults += _T("\" Domain=\"");
		strResults += strDomain.c_str();
		strResults += _T("\" Usage=");

		switch(sidUse)
		{
		default: strResults += _T("Undefined"); break;
		case SidTypeUser: strResults += _T("SidTypeUser"); break;
		case SidTypeGroup: strResults += _T("SidTypeGroup"); break;
		case SidTypeAlias: strResults += _T("SidTypeAlias"); break;
		case SidTypeDomain: strResults += _T("SidTypeDomain"); break;
		case SidTypeInvalid: strResults += _T("SidTypeInvalid"); break;
		case SidTypeUnknown: strResults += _T("SidTypeUnknown"); break;
		case SidTypeComputer: strResults += _T("SidTypeComputer"); break;
		case SidTypeWellKnownGroup: strResults += _T("SidTypeWellKnownGroup"); break;
		case SidTypeDeletedAccount: strResults += _T("SidTypeDeletedAccount"); break;
		}														
	}
	else 
		strResults += _T(" Account=\"\" Domain=\"\" Usage=\"\"");   		

	strResults += _T("/>");
	return strResults;
}
