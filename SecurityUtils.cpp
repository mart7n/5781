#include "SecurityUtils.h"
#include <assert.h>

using namespace IAMC;
using namespace Security;

LoggedOnSid2               Security::g_loggedOnSid;
OpenSecurityDescriptor     Security::g_openSd;

//////////////////////////////////////////////////////////////////////////////
// WinUtils members.
//////////////////////////////////////////////////////////////////////////////
bool WinUtils::IsWinNT()
{
	OSVERSIONINFO osv;

#pragma warning(disable : 4996)
	osv.dwOSVersionInfoSize = sizeof(osv);
	::GetVersionEx(&osv);
#pragma warning(default : 4996)

	return (osv.dwPlatformId == VER_PLATFORM_WIN32_NT);
}

//////////////////////////////////////////////////////////////////////////////
// PublicSecurityDescriptor members.
//////////////////////////////////////////////////////////////////////////////
PublicSecurityDescriptor::PublicSecurityDescriptor()
	: m_pEveryoneSID(NULL), m_pAdminSID(NULL), m_pACL(NULL),
	  m_pSD(NULL), m_pSA(NULL)
{
}

PublicSecurityDescriptor::~PublicSecurityDescriptor()
{
	Cleanup();
}

void PublicSecurityDescriptor::Cleanup()
{
	if (m_pEveryoneSID)
	{
		::FreeSid(m_pEveryoneSID);
		m_pEveryoneSID = NULL;
	}

	if (m_pAdminSID)
	{
		::FreeSid(m_pAdminSID);
		m_pAdminSID = NULL;
	}

	if (m_pACL)
	{
		::LocalFree(m_pACL);
		m_pACL = NULL;
	}

	if (m_pSD)
	{
		::LocalFree(m_pSD);
		m_pSD = NULL;
	}

	m_pSA = NULL;
}

PSECURITY_DESCRIPTOR PublicSecurityDescriptor::GetPSD() const 
{ 
	return m_pSD; 
}

PSECURITY_ATTRIBUTES PublicSecurityDescriptor::GetPSA() const 
{ 
	return m_pSA; 
}

DWORD PublicSecurityDescriptor::CreateOpenSD()
{
    Cleanup();

    DWORD dwResult = ERROR_SUCCESS;

    m_pSD = (PSECURITY_DESCRIPTOR)
        ::LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH); 

    if (::InitializeSecurityDescriptor(m_pSD, SECURITY_DESCRIPTOR_REVISION)) 
    {
		if (!::SetSecurityDescriptorDacl(m_pSD, TRUE, NULL, FALSE))
        {  
            dwResult = ::GetLastError();
        }
        else
        {
            m_sa.nLength = sizeof(SECURITY_ATTRIBUTES);
            m_sa.lpSecurityDescriptor = m_pSD;
            m_sa.bInheritHandle = TRUE;

            m_pSA = &m_sa;
        }
    }
    else
    {
        dwResult = ::GetLastError();
    }

    if (dwResult != ERROR_SUCCESS)
    {
        Cleanup();
        return dwResult;
    }
    else
    {
        return ERROR_SUCCESS;
    }

}

DWORD PublicSecurityDescriptor::CreatePublicSD()
{
    Cleanup();

    DWORD dwResult = ERROR_SUCCESS;

    SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;
    SID_IDENTIFIER_AUTHORITY SIDAuthNT    = SECURITY_NT_AUTHORITY;
    
    if(::AllocateAndInitializeSid(&SIDAuthWorld, 1, SECURITY_WORLD_RID,              
        0, 0, 0, 0, 0, 0, 0, &m_pEveryoneSID)) 
    {           
        EXPLICIT_ACCESS ea[2];

        ::ZeroMemory(&ea, 2 * sizeof(EXPLICIT_ACCESS));
        ea[0].grfAccessPermissions = KEY_READ;
        ea[0].grfAccessMode = SET_ACCESS;
        ea[0].grfInheritance= NO_INHERITANCE;
        ea[0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
        ea[0].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
        ea[0].Trustee.ptstrName  = (LPTSTR)m_pEveryoneSID;

		// Create a SID for the BUILTIN\Administrators group.
        if(::AllocateAndInitializeSid(&SIDAuthNT, 2,
            SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 
            0, 0, 0, 0, 0, 0,&m_pAdminSID))
        {               
            // Initialize an EXPLICIT_ACCESS structure for an ACE.  The ACE 
            // will allow the Administrators group full access to the key.            
            ea[1].grfAccessPermissions = KEY_ALL_ACCESS;
            ea[1].grfAccessMode = SET_ACCESS;
            ea[1].grfInheritance= NO_INHERITANCE;
            ea[1].Trustee.TrusteeForm = TRUSTEE_IS_SID;
            ea[1].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
            ea[1].Trustee.ptstrName  = (LPTSTR) m_pAdminSID;

            dwResult = ::SetEntriesInAcl(2, ea, NULL, &m_pACL);

            if (dwResult == ERROR_SUCCESS)
            {
                m_pSD = (PSECURITY_DESCRIPTOR) 
                    ::LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH); 

                if (::InitializeSecurityDescriptor(m_pSD, 
                    SECURITY_DESCRIPTOR_REVISION)) 
                {
					if (!::SetSecurityDescriptorDacl(m_pSD, TRUE, 
                        NULL, /* m_pACL, */ FALSE))
                    {  
                        dwResult = ::GetLastError();
                    }
                    else
                    {
                        m_sa.nLength = sizeof(SECURITY_ATTRIBUTES);
                        m_sa.lpSecurityDescriptor = m_pSD;
                        m_sa.bInheritHandle = TRUE;

                        m_pSA = &m_sa;

                    }
                }
                else 
                    dwResult = ::GetLastError();       		
            }
        }           
        else 
            dwResult = ::GetLastError();       		
    }
    else 
        dwResult = ::GetLastError();   		

    if (dwResult != ERROR_SUCCESS)
        Cleanup();

    return dwResult;
}

//////////////////////////////////////////////////////////////////////////////
// OpenSecurityDescriptor members.
//////////////////////////////////////////////////////////////////////////////
OpenSecurityDescriptor::OpenSecurityDescriptor()
{
	m_dwError = PublicSecurityDescriptor::CreateOpenSD();
}

DWORD OpenSecurityDescriptor::GetError() const
{
	return m_dwError;
}

//////////////////////////////////////////////////////////////////////////////
// LoggedOnSid members.
//////////////////////////////////////////////////////////////////////////////
LoggedOnSid::LoggedOnSid() : m_pTokenUser(NULL), m_pSid(NULL)
{
}

LoggedOnSid::~LoggedOnSid()
{
	FreeToken();
	FreeSid();
}

void LoggedOnSid::FreeToken()
{
	if (m_pTokenUser)
	{
		delete m_pTokenUser;
		m_pTokenUser = NULL;
	}
}

void LoggedOnSid::FreeSid()
{
	if (m_pSid)
	{
		delete[] m_pSid;
		m_pSid = NULL;
	}
}

PTOKEN_USER LoggedOnSid::GetTokenUser() const
{
	return m_pTokenUser;
}

PSID LoggedOnSid::GetPSid() const
{
	return m_pSid;
}

DWORD LoggedOnSid::GetCurrentProcessUserTokens() 
{
    DWORD dwResult = ERROR_SUCCESS;

    FreeToken();

	HANDLE		hToken;
    DWORD       dwLength = 0;
    if (::OpenProcessToken(::GetCurrentProcess(), TOKEN_QUERY, &hToken))
	{
		::GetTokenInformation(hToken, TokenUser, NULL, 0, &dwLength); 
		m_pTokenUser = (PTOKEN_USER)new BYTE[dwLength];

        if (!::GetTokenInformation(hToken, TokenUser, (LPVOID)m_pTokenUser, 
		    dwLength, &dwLength)) 
		{
            dwResult = ::GetLastError();
            FreeToken();            
		}		
		
        ::CloseHandle(hToken);
	}
    else
    {
        dwResult = ::GetLastError();
    }

	return dwResult;
}

DWORD LoggedOnSid::GetLogOnAccountSID() 
{
    FreeSid();

    DWORD dwResult = ERROR_SUCCESS;

	if (m_pTokenUser || 
        ((dwResult = GetCurrentProcessUserTokens()) == ERROR_SUCCESS))
    {
        assert(m_pTokenUser);

        DWORD dwLength     = 0;
        DWORD cbName       = 0;
        DWORD cbDomain     = 0;

        SID_NAME_USE        sidNameUser;

        ::LookupAccountSid(NULL, m_pTokenUser->User.Sid, NULL, 
            &cbName, NULL, &cbDomain, &sidNameUser);

		LPTSTR lpszName;
		LPTSTR lpszDomain;
		lpszName = new TCHAR[cbName + 1];
		lpszDomain = new TCHAR[cbDomain + 1];

        if (::LookupAccountSid(NULL, m_pTokenUser->User.Sid, lpszName, 
            &cbName, lpszDomain, &cbDomain, &sidNameUser))
		{
            dwLength = ::GetLengthSid(m_pTokenUser->User.Sid);

			m_pSid = (PSID)new BYTE[dwLength];
			
            if (!::CopySid(dwLength, m_pSid, m_pTokenUser->User.Sid))
			{
                dwResult = ::GetLastError();
                FreeSid();
			}			
		}
        else
        {
            dwResult = ::GetLastError();
        }

        delete [] lpszName;
        delete [] lpszDomain;		
    }

    return dwResult;
}

//////////////////////////////////////////////////////////////////////////////
// LoggedOnSid2 members.
//////////////////////////////////////////////////////////////////////////////
LoggedOnSid2::LoggedOnSid2()
{
	m_dwError = LoggedOnSid::GetLogOnAccountSID();
}

DWORD LoggedOnSid2::GetError() const
{
	return m_dwError;
}
