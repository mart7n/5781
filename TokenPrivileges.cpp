#include <assert.h>
#include <atlbase.h>
#include "HeapAllocate.h"
#include "TokenPrivileges.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CTokenPrivileges members.
//////////////////////////////////////////////////////////////////////////////
CTokenPrivileges::CTokenPrivileges(const PTOKEN_PRIVILEGES pOther)
  : m_pTokPrivs(NULL), m_pPrivs(NULL)
{
	*this = pOther;
}

CTokenPrivileges::~CTokenPrivileges() 
{ 
	Free(); 
}

void CTokenPrivileges::Free()
{
	CHeapAllocate::Free(m_pTokPrivs);
	
	if (m_pPrivs)
	{
		delete [] m_pPrivs;
		m_pPrivs = NULL;
	}
}

PTOKEN_PRIVILEGES CTokenPrivileges::operator=(
	const PTOKEN_PRIVILEGES pOther)
{
	if ((PTOKEN_PRIVILEGES)pOther != m_pTokPrivs)
	{
		Free();

		if (pOther)
		{
			TOKEN_PRIVILEGES &otherPrivs = *(TOKEN_PRIVILEGES*)pOther;
			DWORD dwPrivs = otherPrivs.PrivilegeCount;
			DWORD cbPrivs = sizeof(TOKEN_PRIVILEGES) + 
				((dwPrivs-1) * sizeof(LUID_AND_ATTRIBUTES));

			CHeapAllocate allocator(cbPrivs);
			m_pTokPrivs = (PTOKEN_PRIVILEGES)allocator.Detach();

			// Dereference new group pointer and set the group count.						
			TOKEN_PRIVILEGES &newPrivs = *m_pTokPrivs;
			newPrivs.PrivilegeCount = otherPrivs.PrivilegeCount;

			// Create copy of privileges and assign to new privileges list.
			m_pPrivs = new CPrivilege[dwPrivs];
			for (DWORD dwPriv = 0; dwPriv<dwPrivs; dwPriv++)
			{
				m_pPrivs[dwPriv] = &otherPrivs.Privileges[dwPriv].Luid;
				newPrivs.Privileges[dwPriv].Luid = *(PLUID)m_pPrivs[dwPriv];

				newPrivs.Privileges[dwPriv].Attributes = 
					otherPrivs.Privileges[dwPriv].Attributes;
			}
		}
	}

	return m_pTokPrivs;
}

CTokenPrivileges::operator PTOKEN_PRIVILEGES() 
{ 
	return m_pTokPrivs; 
}

CTokenPrivileges::operator const PTOKEN_PRIVILEGES() const 
{ 
	return m_pTokPrivs; 
}

DWORD CTokenPrivileges::GetPrivilegeCount() const
{
	if (!m_pTokPrivs) 
		return 0; 

	return ((TOKEN_PRIVILEGES*)m_pTokPrivs)->PrivilegeCount;
}

PLUID CTokenPrivileges::GetPrivilege(
	DWORD dwIndex, 
	DWORD & dwAttributes) const
{
	DWORD dwPrivs = GetPrivilegeCount();
	if (dwPrivs == 0 || dwIndex >= dwPrivs)
		return NULL;

	dwAttributes = ((TOKEN_PRIVILEGES*)m_pTokPrivs)->Privileges[dwIndex].Attributes;
	return &((TOKEN_PRIVILEGES*)m_pTokPrivs)->Privileges[dwIndex].Luid;
}

bool CTokenPrivileges::DeletePrivilege(DWORD dwIndex)
{
	DWORD dwPrivs = GetPrivilegeCount();
	if (dwPrivs == 0 || dwIndex >= dwPrivs)
		return false;

	// Move privileges up the list
	TOKEN_PRIVILEGES &privs = *(TOKEN_PRIVILEGES*)m_pTokPrivs;
	for (DWORD dwPriv = dwIndex; dwPriv < dwPrivs-2; dwPriv++)
	{
		privs.Privileges[dwPriv].Luid = 
			privs.Privileges[dwPriv+1].Luid;

		privs.Privileges[dwPriv].Attributes =
			privs.Privileges[dwPriv+1].Attributes;
	}

	privs.PrivilegeCount--;
	return true;
}

bool CTokenPrivileges::InsertPrivilege(
	const PLUID pLuid, 
	DWORD attributes, 
	DWORD dwIndex)
{        
	DWORD dwPrivs = GetPrivilegeCount();
	DWORD cbPrivs = sizeof(TOKEN_PRIVILEGES) + 
		((dwPrivs) * sizeof(LUID_AND_ATTRIBUTES));

	CHeapAllocate allocator(cbPrivs);
	TOKEN_PRIVILEGES* pTokPrivs = (TOKEN_PRIVILEGES*)allocator.Detach();

	// Create reference to new privileges count then increment for new 
	// privilege
	DWORD & dwNewPrivs = pTokPrivs->PrivilegeCount;
	dwNewPrivs++;

	// Update to the index to point to the last entry in the list if 
	// it is pointing past the end of the buffer
	if (dwIndex > dwPrivs)
		dwIndex = dwPrivs;

	CPrivilege * pPrivs = new CPrivilege[dwNewPrivs];
	if (m_pTokPrivs)
	{
		TOKEN_PRIVILEGES &oldGroups = *(TOKEN_PRIVILEGES*)m_pTokPrivs;

		// Iterate through old privileges list and copy them to the 
		// new list
		DWORD dwSourcePriv = 0;
		for (DWORD dwDestPriv = 0; dwDestPriv<dwNewPrivs; dwDestPriv++)
		{
			if (dwDestPriv != dwIndex)
			{
				// Copy sid and attributes
				pPrivs[dwDestPriv] = &oldGroups.Privileges[dwSourcePriv].Luid;
				pTokPrivs->Privileges[dwDestPriv].Luid = *(PLUID)pPrivs[dwDestPriv];

				pTokPrivs->Privileges[dwDestPriv].Attributes = 
					oldGroups.Privileges[dwSourcePriv].Attributes;

				dwSourcePriv++;
			}
		}

		Free();
	}

	pPrivs[dwIndex] = (PLUID)pLuid;
	pTokPrivs->Privileges[dwIndex].Attributes = attributes;

	m_pTokPrivs = pTokPrivs;
	m_pPrivs = pPrivs;

	return true;
}

bool CTokenPrivileges::AppendPrivilege(
	const PLUID pLuid, 
	DWORD dwAttributes)            
{ 
	return InsertPrivilege(pLuid, dwAttributes, MAXDWORD); 
}

std::basic_string<TCHAR> CTokenPrivileges::Dump() const
{
	if (!m_pTokPrivs)
		return std::basic_string<TCHAR>(_T("TOKEN GROUPS - Unallocated"));

	std::basic_string<TCHAR> strResults(_T("TOKEN GROUPS - Count: "));

	TCHAR szSize[10];
	DWORD dwCount = GetPrivilegeCount();
	::_ltot_s(dwCount, szSize, 10);
	strResults += szSize;
	strResults += _T(" ");

	CPrivilege priv;
	DWORD dwAttributes;
	for(DWORD dwIndex=0; dwIndex < dwCount; dwIndex++)
	{
		priv = GetPrivilege(dwIndex, dwAttributes);
		strResults += priv.Dump();

		::_ltot_s(dwAttributes, szSize, 16);
		strResults += _T(", attributes(0x0");
		strResults += szSize;
		strResults += _T("), ");
	}

	return strResults;
}

std::basic_string<TCHAR> CTokenPrivileges::DumpXML() const
{
	if (!m_pTokPrivs)
		return std::basic_string<TCHAR>(_T("<TOKEN_PRIVILEGES/>"));

	std::basic_string<TCHAR> strResults(_T("<TOKEN_PRIVILEGES PrivilegeCount="));

	TCHAR szSize[10];
	DWORD dwCount = GetPrivilegeCount();
	::_ltot_s(dwCount, szSize, 10);
	strResults += szSize;

	strResults += _T(">");

	CPrivilege priv;
	DWORD dwAttributes;
	for(DWORD dwIndex=0; dwIndex < dwCount; dwIndex++)
	{
		strResults += _T("<LUID_AND_ATTRIBUTES Attributes=0x0");
		priv = GetPrivilege(dwIndex, dwAttributes);
		::_ltot_s(dwAttributes, szSize, 16);

		strResults += szSize;
		strResults += _T(">");

		strResults += priv.DumpXML();
		strResults += _T("</LUID_AND_ATTRIBUTES>");
	}

	strResults += _T("</TOKEN_PRIVILEGES>");
	return strResults;
}

DWORD CTokenPrivileges::GetSize(const PTOKEN_PRIVILEGES pPrivs)
{
	if (!pPrivs)
		return 0;

	DWORD dwPrivs = ((TOKEN_PRIVILEGES*)pPrivs)->PrivilegeCount;
	return sizeof(PTOKEN_PRIVILEGES) + ((dwPrivs-1) * 
		sizeof(LUID_AND_ATTRIBUTES));            
}
