#include <assert.h>
#include "ProcessSID.h"
#include "AccessToken.h"
#include "ProcessToken.h"

using namespace NTSecurity;

PSID NTSecurity::GetProcessSID()
{
	static CProcessSID sid;
	return sid.GetSID();
}

//////////////////////////////////////////////////////////////////////////////
// CProcessSID members.
//////////////////////////////////////////////////////////////////////////////
CProcessSID::CProcessSID() : m_pSID(NULL)
{
	CProcessToken procToken;
	CAccessToken accessToken(procToken.GetHANDLE());

	m_sid = accessToken.GetUser();
	if (m_sid.IsValid())
		m_pSID = m_sid;			
}

PSID CProcessSID::GetSID() 
{ 
	return m_pSID; 
}

const PSID CProcessSID::GetSID() const 
{ 
	return m_pSID; 
}

CProcessSID::operator PSID() 
{ 
	return GetSID(); 
}

CProcessSID::operator const PSID() const 
{ 
	return GetSID(); 
}
