#pragma once

#include <tchar.h>
#include <windows.h>
#include <string>

namespace NTSecurity
{
	class CStrTok
	{
	public:
		CStrTok(LPCTSTR, LPCTSTR);
		LPCTSTR GetNext();

	private:
		LPCTSTR						m_pszNext;
		std::basic_string<TCHAR>	m_strBuffer;		
	};
}
