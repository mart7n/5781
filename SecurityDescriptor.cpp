#include <assert.h>
#include "SID.h"
#include "HeapAllocate.h"
#include "SecurityDescriptor.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CSecurityDescriptor members.
//////////////////////////////////////////////////////////////////////////////
CSecurityDescriptor::CSecurityDescriptor(const PSECURITY_DESCRIPTOR pSD) 
  : m_pSD(NULL)
{
	*this = pSD;
}

CSecurityDescriptor::CSecurityDescriptor(const CSecurityDescriptor &other)
  : m_pSD(NULL) 
{ 
	*this = other.m_pSD; 
}

CSecurityDescriptor::~CSecurityDescriptor() 
{ 
	Free(); 
}

CSecurityDescriptor & CSecurityDescriptor::operator=(
	const CSecurityDescriptor &other) 
{ 
	*this = other.m_pSD; 
	return *this; 
}

CSecurityDescriptor::operator PSECURITY_DESCRIPTOR() 
{ 
	return m_pSD; 
}

CSecurityDescriptor::operator const PSECURITY_DESCRIPTOR() const 
{ 
	return m_pSD; 
}

void CSecurityDescriptor::Free()
{
	CHeapAllocate::Free(m_pSD);	
}

PSECURITY_DESCRIPTOR CSecurityDescriptor::operator=(
	const PSECURITY_DESCRIPTOR pSD) 
{
	if (pSD != m_pSD)
	{
		Free();

		if (pSD)
		{
			DWORD cbSD = ::GetSecurityDescriptorLength(pSD);
			CHeapAllocate allocator(cbSD);
			m_pSD = allocator.Detach();

			::InitializeSecurityDescriptor(m_pSD, 				
				SECURITY_DESCRIPTOR_REVISION);

			PACL pAcl;
			PSID pSid;          
			BOOL bPresent, bDefault;
			
			// Take copies of the owner sid, group sid, sacl and dacl.  This 
			// is done because the ::SetSecurityDescriptorXXXX() sets the 
			// objects as a reference - we have no guarantee that the 
			// security descriptor passed in will stay in scope for the 
			// duration of this SecurityDescriptor.			
			if (::GetSecurityDescriptorDacl(pSD, &bPresent, &pAcl, &bDefault))
			{
				if (!bPresent)
					pAcl = NULL;

				m_dacl = pAcl;
				::SetSecurityDescriptorDacl(m_pSD, bPresent, (PACL)m_dacl, 
					bDefault);
			}
			else
				m_dacl = NULL;

			if (::GetSecurityDescriptorSacl(pSD, &bPresent, &pAcl, 
				&bDefault))
			{
				if (!bPresent)
					pAcl = NULL;

				m_sacl = pAcl;
				::SetSecurityDescriptorSacl(m_pSD, bPresent, (PACL)m_sacl, 
					bDefault);
			}
			else
				m_sacl = NULL;

			if (::GetSecurityDescriptorOwner(pSD, &pSid, &bDefault))
			{
				m_sidOwner = pSid;
				::SetSecurityDescriptorOwner(m_pSD, (PSID)m_sidOwner, 
					bDefault);
			}
			else
				m_sidOwner = NULL;

			if (::GetSecurityDescriptorGroup(pSD, &pSid, &bDefault))
			{
				m_sidGroup = pSid;
				::SetSecurityDescriptorGroup(m_pSD, (PSID)m_sidGroup, 
					bDefault);
			}
			else
				m_sidGroup = NULL;
		}   
	}

	return m_pSD;
}

void CSecurityDescriptor::CreateEmpty()
{
	Free();

	CHeapAllocate allocator(sizeof(SECURITY_DESCRIPTOR));
	m_pSD = allocator.Detach();

	::InitializeSecurityDescriptor(m_pSD, SECURITY_DESCRIPTOR_REVISION);
}

 /**
 ****************************************************************************
 * Ensures a security descriptor is available by creating a default one 
 * if one does not exist.
 ****************************************************************************
 */
void CSecurityDescriptor::EnsureAvailable()
{
	if (!m_pSD)
	{
		CHeapAllocate allocator(sizeof(SECURITY_DESCRIPTOR));
		m_pSD = allocator.Detach();
		::InitializeSecurityDescriptor(m_pSD, SECURITY_DESCRIPTOR_REVISION);
	}
}

 /**
 ****************************************************************************
 * Sets the DACL for the security descriptor to the default - meaning 
 * that if will be obtained from the creator's access token.
 ****************************************************************************
 */
bool CSecurityDescriptor::SetDACLDefault()
{
	EnsureAvailable();
	return ::SetSecurityDescriptorDacl(m_pSD, TRUE, NULL, TRUE) == TRUE;
}

 /**
 ****************************************************************************
 * Sets the DACL for the security descriptor.  If this is NULL then this 
 * will set an empty DACL - meaning that everyone will have access.
 ****************************************************************************
 */
bool CSecurityDescriptor::SetDACL(const PACL pAcl)
{
	EnsureAvailable();

	m_dacl = pAcl;
	PACL pDacl = m_dacl;
	return (::SetSecurityDescriptorDacl(m_pSD, pDacl != NULL, 
		(PACL)m_dacl, FALSE) == TRUE);
}

 /**
 ****************************************************************************
 * Returns the DACL from the security descriptor.   For an empty DACL, 
 * this will be NULL (implying that everyone has access).  bIsDefault will be
 * set to true if the DACL is a default DACL - meaning that it is obtained
 * from the owners access token.
 ****************************************************************************
 */
PACL CSecurityDescriptor::GetDACL() const
{
	if (!IsPresentDACL())
		return NULL;

	return ((SECURITY_DESCRIPTOR*)m_pSD)->Dacl;
}

 /**
 ****************************************************************************
 * Returns true if the DACL is a default DACL - meaning that it is 
 * obtained from the owners access token.
 ****************************************************************************
 */
bool CSecurityDescriptor::IsDefaultDACL() const
{
	if (!m_pSD)
		return false;

	return (((SECURITY_DESCRIPTOR*)m_pSD)->Control & SE_DACL_DEFAULTED) !=0;
}

 /**
 ****************************************************************************
 * Returns true if a DACL is present (if false then this means everyone 
 * has access).
 ****************************************************************************
 */
bool CSecurityDescriptor::IsPresentDACL() const
{
	if (!m_pSD)
		return false;

	return (((SECURITY_DESCRIPTOR*)m_pSD)->Control & SE_DACL_PRESENT) != 0;
}

 /**
 ****************************************************************************
 * Sets the SACL for the security descriptor to the default - meaning 
 * that if will be obtained from the creator's access token.
 ****************************************************************************
 */
bool CSecurityDescriptor::SetSACLDefault()
{
	EnsureAvailable();
	return ::SetSecurityDescriptorSacl(m_pSD, TRUE, NULL, TRUE) == TRUE;
}

 /**
 ****************************************************************************
 * Sets the SACL for the security descriptor.  If this is NULL then this 
 * will set an empty SACL - no auditing will take place.
 ****************************************************************************
 */
bool CSecurityDescriptor::SetSACL(const PACL pAcl)
{
	EnsureAvailable();

	m_sacl = pAcl;
	PACL pSacl = m_sacl;
	return (::SetSecurityDescriptorSacl(m_pSD, pSacl != NULL, pAcl, 
		FALSE) == TRUE);
}

 /**
 ****************************************************************************
 * Returns the SACL from the security descriptor.   For an empty SACL, 
 * this will be NULL (implying that no auditing will take place).
 ****************************************************************************
 */
PACL CSecurityDescriptor::GetSACL() const
{
	if (!IsPresentSACL())
		return NULL;

	return ((SECURITY_DESCRIPTOR*)m_pSD)->Sacl;
}

 /**
 ****************************************************************************
 * Returns true if the SACL is a default SACL - meaning that it is 
 * obtained from the owners access token.
 ****************************************************************************
 */
bool CSecurityDescriptor::IsDefaultSACL() const
{
	if (!m_pSD)
		return false;

	return (((SECURITY_DESCRIPTOR*)m_pSD)->Control & SE_SACL_DEFAULTED) !=0;
}

 /**
 ****************************************************************************
 * Returns true if a SACL is present (if false then no auditing will take
 * place.
 ****************************************************************************
 */
bool CSecurityDescriptor::IsPresentSACL() const
{
	if (!m_pSD)
		return false;

	return (((SECURITY_DESCRIPTOR*)m_pSD)->Control & SE_SACL_PRESENT) != 0;
}

 /**
 ****************************************************************************
 * Sets the owner sid for the security descriptor to default.  This 
 * means is will be obtained from the owners access token.
 ****************************************************************************
 */
bool CSecurityDescriptor::SetSIDOwnerDefault()
{
	EnsureAvailable();
	return ::SetSecurityDescriptorOwner(m_pSD, NULL, TRUE) == TRUE;
}

 /**
 ****************************************************************************
 * Sets the owner sid for the security descriptor.  If this passed NULL 
 * then the owner sid will be set to default (see SetSIDOwnerDefault()).
 ****************************************************************************
 */
bool CSecurityDescriptor::SetSIDOwner(
	const PSID pSid)
{
	EnsureAvailable();
	m_sidOwner = pSid;

	if (!pSid)
		return SetSIDOwnerDefault();
	else
	{
		return (::SetSecurityDescriptorOwner(m_pSD, (PSID)m_sidOwner, FALSE) 
			== TRUE);
	}
}

 /**
 ****************************************************************************
 * Returns true if the owner sid is default (meaning that the sid is 
 * obtained from the owners access token).
 ****************************************************************************
 */
bool CSecurityDescriptor::IsDefaultSIDOwner() const
{
	if (!m_pSD)
		return false;

	return (((SECURITY_DESCRIPTOR*)m_pSD)->Control & SE_OWNER_DEFAULTED) != 0;
}
 
PSID CSecurityDescriptor::GetSIDOwner() const
{
	if (!m_pSD)
		return NULL;

	return ((SECURITY_DESCRIPTOR*)m_pSD)->Owner;
}

 /**
 ****************************************************************************
 * Sets the group sid for the security descriptor to default.  This 
 * means is will be obtained from the owners access token.
 ****************************************************************************
 */
bool CSecurityDescriptor::SetSIDGroupDefault()
{
	EnsureAvailable();
	return ::SetSecurityDescriptorGroup(m_pSD, NULL, TRUE) == TRUE;
}

 /**
 ****************************************************************************
 * Sets the group sid for the security descriptor.  If this passed NULL 
 * then the group sid will be set to default (see SetSIDGroupDefault()).
 ****************************************************************************
 */
bool CSecurityDescriptor::SetSIDGroup(
	const PSID pSid)
{
	EnsureAvailable();
	m_sidGroup = pSid;

	if (!pSid)
		return SetSIDGroupDefault();
	else
	{
		return ::SetSecurityDescriptorGroup(m_pSD, (PSID)m_sidGroup, FALSE) 
			== TRUE;
	}
}

 /**
 ****************************************************************************
 * Returns true if the group sid is default (meaning that the sid is 
 * obtained from the owners access token).
 ****************************************************************************
 */
bool CSecurityDescriptor::IsDefaultSIDGroup() const
{
	if (!m_pSD)
		return false;

	return (((SECURITY_DESCRIPTOR*)m_pSD)->Control & SE_GROUP_DEFAULTED) !=0;
}

PSID CSecurityDescriptor::GetSIDGroup() const
{
	if (!m_pSD)
		return NULL;

	return ((SECURITY_DESCRIPTOR*)m_pSD)->Group;
}

 /**
 ****************************************************************************
 * Function to obtain security information for a given object.
 * This function wraps a call to ::GetSecurityInfo() and if successfull, puts
 * a copy of the returned security descriptor in the security descriptor.  
 * The dwSecurityInfo parameter specifies what information is required in the
 * security descriptor and can be calculated by 'OR'ing values from the info 
 * enum (which maps directly to the win32 SECURITY_INFORMATION structure).
 * If an false is returned then ::GetLastError() can be used to obtain 
 * details.
 ****************************************************************************
 */
bool CSecurityDescriptor::GetFromObject(
	HANDLE hObject, 
	SE_OBJECT_TYPE objectType, 
	DWORD dwSecurityInfo)
{	
	PACL pDacl = NULL;
	PACL pSacl = NULL;
	PSID pSidOwner = NULL;
	PSID pSidGroup = NULL;	
	PSECURITY_DESCRIPTOR pSD;

	DWORD dwResult = ::GetSecurityInfo(hObject, objectType, dwSecurityInfo, 
		&pSidOwner, &pSidGroup, &pDacl, &pSacl, &pSD);

	if (dwResult == ERROR_SUCCESS)
	{
		*this = pSD;
		return true;
	}

	return false;
}

 /**
 ****************************************************************************
 * Function to set security information for a given object from the
 * security descriptor.
 * This function wraps a call to ::SetSecurityInfo() and if successfull, 
 * updates the security descriptor for a specified object. 
 * The dwSecurityInfo parameter specifies what information requires updating
 * in the security descriptor and can be calculated by 'OR'ing values from 
 * the info  enum (which maps directly to the win32 SECURITY_INFORMATION 
 * structure).
 * If an false is returned then ::GetLastError() can be used to obtain 
 * details.
 ****************************************************************************
 */
bool CSecurityDescriptor::SetToObject(
	HANDLE hObject, 
	SE_OBJECT_TYPE objectType, 
	DWORD dwSecurityInfo) const
{	
	PACL pDacl = GetDACL();
	PACL pSacl = GetSACL();
	PSID pSidOwner = GetSIDOwner();
	PSID pSidGroup = GetSIDGroup();

	DWORD dwResult = ::SetSecurityInfo(hObject, objectType, dwSecurityInfo, 
		pSidOwner, pSidGroup, pDacl, pSacl);

	return (dwResult == ERROR_SUCCESS);		
}

bool CSecurityDescriptor::GetFromFile(HANDLE hFile, DWORD dwSecurityInfo)
{ 
	return GetFromObject(hFile, SE_FILE_OBJECT, dwSecurityInfo); 
}

bool CSecurityDescriptor::GetFromFile(
	LPCTSTR lpszFile, 
	DWORD dwSecurityInfo)
{	
	// Calculate required access to the file.
	// Note:  To gain access to a security access control list (SACL), a 
	// process must have the SE_SECURITY_NAME privilege. When requesting 
	// access, the calling process must request ACCESS_SYSTEM_SECURITY in the 
	// 	desired access mask. ("Manage auditing and security log" under user 
	// rights in user manager).
	
	DWORD dwAccess = 
		((dwSecurityInfo & (UpdateDacl | UpdateSacl | UpdateGroup))
			? READ_CONTROL : 0) |             
			((dwSecurityInfo & UpdateSacl) != 0 ? ACCESS_SYSTEM_SECURITY : 0);

	HANDLE hFile = ::CreateFile(lpszFile, dwAccess, 
		0, //FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (hFile != INVALID_HANDLE_VALUE)
	{
		bool bResult = GetFromFile(hFile, dwSecurityInfo);
		::CloseHandle(hFile);
		return bResult;
	}

	return false;
}

bool CSecurityDescriptor::SetToFile(
	LPCTSTR lpszFile, 
	DWORD dwSecurityInfo) const
{	
	// Calculate required access to the file.
	// Note:  To gain access to a security access control list (SACL), a 
	// process must have the SE_SECURITY_NAME privilege. When requesting 
	// access, the calling process must request ACCESS_SYSTEM_SECURITY in the 
	// desired access mask. ("Manage auditing and security log" under user 
	// rights in user manager).	
	DWORD dwAccess = 
		(dwSecurityInfo & UpdateDacl ? WRITE_DAC : 0) |
		(dwSecurityInfo & UpdateSacl ? ACCESS_SYSTEM_SECURITY : 0) |
		(dwSecurityInfo & UpdateGroup ? WRITE_OWNER : 0) | // ????TODO
		(dwSecurityInfo & UpdateOwner ? WRITE_OWNER : 0);

	HANDLE hFile = ::CreateFile(lpszFile, dwAccess, 
		FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (hFile != INVALID_HANDLE_VALUE)
	{
		bool bResult = SetToFile(hFile, dwSecurityInfo);
		::CloseHandle(hFile);
		return bResult;
	}

	return false;
}

bool CSecurityDescriptor::SetToFile(
	HANDLE hFile, 
	DWORD dwSecurityInfo) const
{ 
	return SetToObject(hFile, SE_FILE_OBJECT, dwSecurityInfo); 
}

bool CSecurityDescriptor::GetFromService(
	HANDLE hService, 
	DWORD dwSecurityInfo)
{ 
	return GetFromObject(hService, SE_SERVICE, dwSecurityInfo); 
}

bool CSecurityDescriptor::GetFromPrinter(
	HANDLE hPrinter, 
	DWORD dwSecurityInfo)
{ 
	return GetFromObject(hPrinter, SE_PRINTER, dwSecurityInfo); 
}

bool CSecurityDescriptor::SetToService(
	HANDLE hService, 
	DWORD dwSecurityInfo) const
{ 
	return SetToObject(hService, SE_SERVICE, dwSecurityInfo); 
}

bool CSecurityDescriptor::SetToPrinter(
	HANDLE hPrinter, 
	DWORD dwSecurityInfo) const
{ 
	return SetToObject(hPrinter, SE_PRINTER, dwSecurityInfo); 
}

bool CSecurityDescriptor::SetToRegistyKey(
	HANDLE hKey, 
	DWORD dwSecurityInfo) const
{ 
	return SetToObject(hKey, SE_REGISTRY_KEY, dwSecurityInfo); 
}

bool CSecurityDescriptor::SetToNetworkShare(
	HANDLE hLMShare, 
	DWORD dwSecurityInfo) const
{ 
	return SetToObject(hLMShare, SE_LMSHARE, dwSecurityInfo); 
}

bool CSecurityDescriptor::SetToKernelObject(
	HANDLE hKernObject, 
	DWORD dwSecurityInfo) const
{ 
	return SetToObject(hKernObject, SE_KERNEL_OBJECT, dwSecurityInfo); 
}

bool CSecurityDescriptor::SetToWindowObject(
	HANDLE hWndObject, 
	DWORD dwSecurityInfo) const
{ 
	return SetToObject(hWndObject, SE_WINDOW_OBJECT,  dwSecurityInfo); 
}

bool CSecurityDescriptor::GetFromRegistryKey(
	HKEY hKeyRoot, 
	LPCTSTR lpszSubKey, 
	DWORD dwSecurityInfo)
{	
	// Calculate required access to the file.
	// Note:  To gain access to a security access control list (SACL), a 
	// process must have the SE_SECURITY_NAME privilege. When requesting 
	// access, the calling process must request ACCESS_SYSTEM_SECURITY in the 
	// desired access mask. ("Manage auditing and security log" under user 
	// rights in user manager).	
	DWORD dwAccess = KEY_QUERY_VALUE |
		((dwSecurityInfo & (UpdateDacl | UpdateSacl | UpdateGroup))
			? READ_CONTROL : 0) |             
			((dwSecurityInfo & UpdateSacl) != 0 ? ACCESS_SYSTEM_SECURITY : 0);

	HKEY hKey;
	if (ERROR_SUCCESS == ::RegOpenKeyEx(hKeyRoot, lpszSubKey, 0, dwAccess, 
		&hKey))
	{
		bool bResult = GetFromRegistryKey(hKey, dwSecurityInfo);
		::RegCloseKey(hKey);
		return bResult;
	}

	return false;
}

bool CSecurityDescriptor::GetFromRegistryKey(
	HANDLE hKey, 
	DWORD dwSecurityInfo)
{ 
	return GetFromObject(hKey, SE_REGISTRY_KEY, dwSecurityInfo); 
}

bool CSecurityDescriptor::GetFromNetworkShare(
	HANDLE hLMShare
	, DWORD dwSecurityInfo)
{ 
	return GetFromObject(hLMShare, SE_LMSHARE, dwSecurityInfo); 
}

bool CSecurityDescriptor::GetFromKernelObject(
	HANDLE hKernObject, 
	DWORD dwSecurityInfo)
{ 
	return GetFromObject(hKernObject, SE_KERNEL_OBJECT, dwSecurityInfo); 
}

bool CSecurityDescriptor::GetFromWindowObject(
	HANDLE hWndObject, 
	DWORD dwSecurityInfo)
{ 
	return GetFromObject(hWndObject, SE_WINDOW_OBJECT, dwSecurityInfo); 
}

bool CSecurityDescriptor::SetToRegistryKey(
	HKEY hKeyRoot, 
	LPCTSTR lpszSubKey, 
	DWORD dwSecurityInfo)
{
	// Calculate required access to the file.
	// Note:  To gain access to a security access control list (SACL), a 
	// process must have the SE_SECURITY_NAME privilege. When requesting 
	// access, the calling process must request ACCESS_SYSTEM_SECURITY in the 
	// desired access mask. ("Manage auditing and security log" under user 
	// rights in user manager).
	DWORD dwAccess = KEY_QUERY_VALUE |
		((dwSecurityInfo & (UpdateDacl | UpdateSacl | UpdateGroup))
			? READ_CONTROL : 0) |             
			((dwSecurityInfo & UpdateSacl) != 0 ? ACCESS_SYSTEM_SECURITY : 0);

	HKEY hKey;
	if (ERROR_SUCCESS == ::RegOpenKeyEx(hKeyRoot, lpszSubKey, 0, dwAccess, &hKey))
	{
		bool bResult = SetToRegistyKey(hKey, dwSecurityInfo);
		::RegCloseKey(hKey);
		return bResult;
	}

	return false;
}

std::basic_string<TCHAR> CSecurityDescriptor::Dump() const
{
	if (!m_pSD)
	{
		return std::basic_string<TCHAR>(
			_T("SECURITY DESCRIPTOR - Unallocated"));
	}

	CSID sid;
	CACL acl;

	std::basic_string<TCHAR> strDefault(_T("default"));
	std::basic_string<TCHAR> strResult(_T("SECURITY DESCRIPTOR - Owner "));

	if (IsDefaultSIDOwner())
		strResult += strDefault;
	else
	{
		sid = GetSIDOwner();
		strResult += sid.Dump().c_str();
	}

	strResult += _T(", Group ");
	if (IsDefaultSIDGroup())
		strResult += strDefault;
	else
	{
		sid = GetSIDGroup();
		strResult += sid.Dump().c_str();
	}

	strResult += _T(", Descretionary ");
	if (IsDefaultDACL())
		strResult += strDefault;
	else
	{
		acl = GetDACL();
		strResult += acl.Dump().c_str();
	}

	strResult += _T(", Audit ");
	if (IsDefaultSACL())
		strResult += strDefault;
	else
	{
		acl = GetSACL();
		strResult += acl.Dump().c_str();
	}

	return strResult;
}

std::basic_string<TCHAR> CSecurityDescriptor::DumpXML() const
{
	if (!m_pSD)
		return std::basic_string<TCHAR>(_T("<SECURITY_DESCRIPTOR/>"));

	CSID sid;
	CACL acl;

	std::basic_string<TCHAR> strDefault(_T("default"));
	std::basic_string<TCHAR> strResult(_T("<SECURITY_DESCRIPTOR>"));

	strResult += _T("<OwnerSID>");
	if (IsDefaultSIDOwner())
		strResult += strDefault;
	else
	{
		sid = GetSIDOwner();
		strResult += sid.DumpXML().c_str();
	}
	strResult += _T("</OwnerSID>");

	strResult += _T("<GroupSID>");
	if (IsDefaultSIDGroup())
		strResult += strDefault;
	else
	{
		sid = GetSIDGroup();
		strResult += sid.DumpXML().c_str();
	}
	strResult += _T("</GroupSID>");

	strResult += _T("<DACL>");	
	if (IsDefaultDACL())
		strResult += strDefault;
	else
	{
		acl = GetDACL();
		strResult += acl.DumpXML().c_str();
	}
	strResult += _T("</DACL>");

	strResult += _T("<SACL>");
	if (IsDefaultSACL())
		strResult += strDefault;
	else
	{
		acl = GetSACL();
		strResult += acl.DumpXML().c_str();
	}
	strResult += _T("</SACL>");

	strResult += _T("</SECURITY_DESCRIPTOR>");
	return strResult;
}
