#pragma once

#include <windows.h>
#include <tchar.h>
#include <string>
#include <aclapi.h>
#include "ACE.h"

namespace NTSecurity
{
	/**
	****************************************************************************
	* Access Control Lists (ACL) - An Access control list is a list of 
	* access control entries.  There are two types of access control lists:
	* 1).  Discretional Access Control List (DACL) - a DACL specifies access to 
	*	   a resource an can contain allowed or denied access control entries,
	* 2).  System Access Control List (SACL) - a SACL specifies auditing of events
	*	   and can contain system audit access control entries.</P>	
	****************************************************************************
	*/
	class CACL
	{
	public:
		CACL(const CACL &);		
		CACL(const PACL = NULL);
		~CACL();

		PACL operator=(const PACL);
		CACL & operator=(const CACL &);

		operator PACL();
		operator const PACL() const;

		DWORD GetSize() const;
		BYTE GetRevision() const;
		PACE GetACE(DWORD) const;
		DWORD GetACECount() const;
		
		bool DeleteACE(DWORD);
		bool AppendACE(const PACE);
		bool InsertACE(const PACE, DWORD);	

		int ExtendACL(DWORD);
		void CreateEmptyACL();	
		DWORD RemoveInvalidACES();

		bool ReOrder();
		bool IsValid(DWORD &) const;

		std::basic_string<TCHAR> Dump() const;
		std::basic_string<TCHAR> DumpXML() const;

	private:
		void Free();
		bool InsertACE(const PACE, DWORD, DWORD);		
		static int ExtendAndCopyACL(const PACL, DWORD, PACL &);

		PACL m_pAcl;
	};
}
