#include <assert.h>
#include <atlbase.h>
#include "TokenGroups.h"
#include "HeapAllocate.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CTokenGroups members.
//////////////////////////////////////////////////////////////////////////////
CTokenGroups::CTokenGroups(const PTOKEN_GROUPS pGroups)
  : m_pGroups(NULL), m_pSids(NULL) 
{ 
	*this = pGroups; 
}

CTokenGroups::~CTokenGroups() 
{ 
	Free(); 
}

void CTokenGroups::Free()
{
	CHeapAllocate::Free(m_pGroups);
	
	if (m_pSids)
	{
		delete [] m_pSids;
		m_pSids = NULL;
	}
}

PTOKEN_GROUPS CTokenGroups::operator=(const PTOKEN_GROUPS pOther)
{
	if ((PTOKEN_GROUPS)pOther != m_pGroups)
	{
		Free();

		if (pOther)
		{
			TOKEN_GROUPS &otherGroup = *(TOKEN_GROUPS*)pOther;            
			DWORD dwGroups = otherGroup.GroupCount;
			DWORD cbGroups = sizeof(TOKEN_GROUPS) + 
				((dwGroups-1) * sizeof(SID_AND_ATTRIBUTES));

			CHeapAllocate allocator(cbGroups);
			m_pGroups = (PTOKEN_GROUPS)allocator.Detach();

			TOKEN_GROUPS &newGroup = *m_pGroups;
			newGroup.GroupCount = otherGroup.GroupCount;

			// Create copy of sids and assign to new groups						
			m_pSids = new CSID[dwGroups];
			for (DWORD dwSid = 0; dwSid<dwGroups; dwSid++)
			{
				m_pSids[dwSid] = otherGroup.Groups[dwSid].Sid;

				newGroup.Groups[dwSid].Sid = (PSID)m_pSids[dwSid];
				newGroup.Groups[dwSid].Attributes = 
					otherGroup.Groups[dwSid].Attributes;

			}
		}
	}

	return m_pGroups;
}

CTokenGroups::operator PTOKEN_GROUPS() 
{ 
	return m_pGroups; 
}

CTokenGroups::operator const PTOKEN_GROUPS() const 
{ 
	return m_pGroups; 
}

DWORD CTokenGroups::GetSIDCount() const
{
	if (!m_pGroups) 
		return 0; 

	return ((TOKEN_GROUPS*)m_pGroups)->GroupCount;
}

PSID CTokenGroups::GetSID(
	DWORD dwIndex, 
	DWORD & dwAttributes) const
{
	DWORD dwSids = GetSIDCount();
	if (dwSids == 0 || dwIndex >= dwSids)
		return NULL;

	dwAttributes = ((TOKEN_GROUPS*)m_pGroups)->Groups[dwIndex].Attributes;
	return ((TOKEN_GROUPS*)m_pGroups)->Groups[dwIndex].Sid;
}

/*
*********************************************************************
* Obtain the logged on sid (See "mk:@MSITStore:d:\MSDN98\98VSa\1033\
* winbase.chm::/devdoc/live/pdwbase/accclsrv_1pk4.htm" in the MSDN).
*********************************************************************
*/
PSID CTokenGroups::GetLogonSID(
	DWORD & dwIndex, 
	DWORD & dwAttributes) const
{   
	PSID pResult = NULL;
	DWORD dwSids = GetSIDCount();
	for (dwIndex = 0; dwIndex < dwSids; dwIndex++)
	{
		PSID pSid = GetSID(dwIndex, dwAttributes);
		if (pSid)
		{
			if ((dwAttributes & SE_GROUP_LOGON_ID) == SE_GROUP_LOGON_ID)
			{
				pResult = pSid;
				break;
			}
		}
	}

	return pResult;
}

bool CTokenGroups::DeleteSID(DWORD dwIndex)
{
	DWORD dwSids = GetSIDCount();
	if (dwSids == 0 || dwIndex >= dwSids)
		return false;

	// Move sids up the list
	TOKEN_GROUPS &groups = *(TOKEN_GROUPS*)m_pGroups;
	for (DWORD dwSid = dwIndex; dwSid < dwSids-2; dwSid++)
	{
		groups.Groups[dwSid].Sid = groups.Groups[dwSid+1].Sid;
		groups.Groups[dwSid].Attributes = 
			groups.Groups[dwSid+1].Attributes;
	}

	groups.GroupCount--;
	return true;
}

bool CTokenGroups::InsertSID(
	const PSID pSid, 
	DWORD dwAttributes, 
	DWORD dwIndex)
{   
	// Allocate memory for the new group (including room for new sid)
	DWORD dwGroups = GetSIDCount();				
	DWORD cbGroups = sizeof(TOKEN_GROUPS) + 
		((dwGroups) * sizeof(SID_AND_ATTRIBUTES));

	CHeapAllocate groups;
	TOKEN_GROUPS* pGroups = (TOKEN_GROUPS*)groups.Alloc(cbGroups);

	// Create reference to new group count then increment for new sid
	DWORD & dwNewGroups = pGroups->GroupCount;
	dwNewGroups++;

	// Update to the index to point to the last entry in the list if 
	// it is pointing past the end of the buffer				
	if (dwIndex > dwGroups)
		dwIndex = dwGroups;

	CSID * pSids = new CSID[dwNewGroups];
	if (m_pGroups)
	{
		TOKEN_GROUPS &oldGroups = *(TOKEN_GROUPS*)m_pGroups;

		DWORD dwSourceSid = 0;
		for (DWORD dwDestSid = 0; dwDestSid<dwNewGroups; dwDestSid++)
		{
			if (dwDestSid != dwIndex)
			{
				// Copy sid and attributes
				pSids[dwDestSid] = oldGroups.Groups[dwSourceSid].Sid;

				pGroups->Groups[dwDestSid].Sid = 
					(PSID)pSids[dwDestSid];

				pGroups->Groups[dwDestSid].Attributes = 
					oldGroups.Groups[dwSourceSid].Attributes;

				dwSourceSid++;
			}
		}

		Free();
	}

	// Insert new sid and attributes into new slot
	pSids[dwIndex] = (PSID)pSid;
	pGroups->Groups[dwIndex].Attributes = dwAttributes;

	m_pGroups = (TOKEN_GROUPS*)groups.Detach();
	m_pSids = pSids;

	return true;
}

bool CTokenGroups::AppendSID(
	const PSID pSid, 
	DWORD dwAttributes)            			
{ 
	return InsertSID(pSid, dwAttributes, MAXDWORD); 
}

DWORD CTokenGroups::GetSize() const 
{ 
	return GetSize(m_pGroups); 
}

/*
*********************************************************************
* Search for specified sid within the group.  This search starts at 
* the position based on a zero based index specified by the dwIndex 
* parameter (for example call FindSID(pSid, 0) to start the search 
* from start of the list.)
* If the search is successful the index of the sid will be returned 
* in dwIndex.  This can then be used in a call the 
* CTokenGroups::GetSID(). 
*********************************************************************
*/
bool CTokenGroups::FindSID(
	const PSID pSid, 
	DWORD & dwIndex) const
{
	if (!m_pGroups || !pSid)
		return false;

	CSID sid;   
	DWORD dwAttribs;
	for (; dwIndex < GetSIDCount(); dwIndex++)
	{
		sid = GetSID(dwIndex, dwAttribs);
		if (sid == pSid)
			return true;
	}

	return false;
}

bool CTokenGroups::IsSIDEnabled(const PSID pSid) const
{
	if (!m_pGroups)
		return false;

	PSID pSidOther;
	DWORD dwAttribs;
	for(DWORD dwIndex = 0; FindSID(pSid, dwIndex); dwIndex++)
	{
		pSidOther = GetSID(dwIndex, dwAttribs);

		// Ignore any NULL entries
		if (pSidOther)
		{
			if (dwAttribs & SE_GROUP_ENABLED) 
				return true;
		}               
	}

	return false;
}

std::basic_string<TCHAR> CTokenGroups::Dump() const
{
	if (!m_pGroups)
		return std::basic_string<TCHAR>(_T("TOKEN GROUPS - Unallocated"));

	std::basic_string<TCHAR> strResults(_T("TOKEN GROUPS - Count: "));

	TCHAR szSize[10];
	DWORD dwCount = GetSIDCount();
	::_ltot_s(dwCount, szSize, 10);
	strResults += szSize;

	CSID sid;
	DWORD dwAttributes;
	for(DWORD dwIndex=0; dwIndex < dwCount; dwIndex++)
	{
		sid = GetSID(dwIndex, dwAttributes);
		strResults += sid.Dump();

		::_ltot_s(dwAttributes, szSize, 16);
		strResults += _T(", attributes(0x0");
		strResults += szSize;
		strResults += _T("), ");
	}

	return strResults;
}

std::basic_string<TCHAR> CTokenGroups::DumpXML() const
{
	if (!m_pGroups)
		return std::basic_string<TCHAR>(_T("<TOKEN_GROUPS/>"));

	std::basic_string<TCHAR> strResults(_T("<TOKEN_GROUPS GroupCount="));

	TCHAR szSize[10];
	DWORD dwCount = GetSIDCount();
	::_ltot_s(dwCount, szSize, 10);
	strResults += szSize;
	strResults += _T(">");

	CSID sid;
	DWORD dwAttributes;
	for(DWORD dwIndex=0; dwIndex < dwCount; dwIndex++)
	{
		strResults += _T("<SID_AND_ATTRIBUTES Attributes=0x0");
		sid = GetSID(dwIndex, dwAttributes);
		::_ltot_s(dwAttributes, szSize, 16);
		strResults += szSize;

		strResults += _T(">");
		strResults += sid.DumpXML();
		strResults += _T("</SID_AND_ATTRIBUTES>");
	}

	strResults += _T("</TOKEN_GROUPS>");

	return strResults;
}

DWORD CTokenGroups::GetSize(PTOKEN_GROUPS pGroups)
{
	if (!pGroups)
		return 0;

	DWORD dwGroups = ((TOKEN_GROUPS*)pGroups)->GroupCount;
	return sizeof(TOKEN_GROUPS) + ((dwGroups-1) * 
		sizeof(SID_AND_ATTRIBUTES));            
}
