#pragma once

#include <windows.h>
#include <tchar.h>
#include <string>
#include <aclapi.h>

namespace NTSecurity
{
	/**
	****************************************************************************
	* Security Identifiers - a security identifier (SID) identifies a 
	* trustee (this can include users/groups). 
	*
	* A SID consists of:
	*  a revision level of the sid,
	*  an identifier authority - this defines the authority on which the sid 
	*      was allocated,
	*  a variable number of subauthorities (also known as RIDs or relative 
	*      identifiers) that uniquely identify the trustee relative to the
	*      authority on which the sid was allocated.
	*
	*
	* SIDs can be represented in standardized string notation for SIDs, which 
	* makes it simpler to visualize their components: 
	*
	*   
	*   S-R-I-S-S...
	*
	* In this notation, the first literal character S identifies the series of 
	* digits as a SID, R is the revision level, I is the identifier-authority 
	* value, and S... is one or more subauthority values. 
	*
	* The following example uses this notation to display the well-known 
	* domain-relative SID of the local Administrators group:
	*
	* S-1-5-32-544
	* 
	* In this example, the SID has the following components. The constants in 
	* parentheses are well-known identifier authority and RID values defined 
	* in WINNT.H. 
	*
	* A revision level of 1 
	* An identifier-authority value of 5 (SECURITY_NT_AUTHORITY) 
	* A first subauthority value of 32 (SECURITY_BUILTIN_DOMAIN_RID) 
	* A second subauthority value of 544 (DOMAIN_ALIAS_RID_ADMINS).
	*
	* This could be allocated with a call to CreateSID() using the following
	* syntax:
	*
	*      SID_IDENTIFIER_AUTHORITY auth = SECURITY_NT_AUTHORITY;
	*
	*      CreateSID(auth, 2, SECURITY_BUILTIN_DOMAIN_RID, 
	*          DOMAIN_ALIAS_RID_ADMINS);
	*
	* Or with the CreateNTSID() function (designed specifically for creating
	* NT type sids (which use the SECURITY_NT_AUTHORITY identifier authority):
	*
	*      CreateNTSID(2, SECURITY_BUILTIN_DOMAIN_RID, 
	*          DOMAIN_ALIAS_RID_ADMINS);
	****************************************************************************
	*/
	class CSID
	{
	public:
		CSID(const CSID &);			
		CSID(const PSID = NULL);
		CSID(LPCTSTR, LPCTSTR = NULL);
		~CSID();
		
		PSID operator=(const PSID);
		CSID & operator=(const CSID &);		

		bool operator==(const PSID);

		operator PSID();
		operator const PSID() const;

		void CreateSID(const SID_IDENTIFIER_AUTHORITY &, BYTE, ...);	

		// Also see macros after definition of class for generating well known
		// sids and sids for standard NT groups
		void CreateNTSID(BYTE nSubAuthorityCount = 0, ...);
		void CreateNullSID(BYTE nSubAuthorityCount = 0, ...);
		void CreateWorldSID(BYTE nSubAuthorityCount = 0, ...);
		void CreateLocalSID(BYTE nSubAuthorityCount = 0, ...);
		void CreateCreatorSID(BYTE nSubAuthorityCount = 0, ...);

		bool SetAccount(LPCTSTR, LPCTSTR);

		bool GetAccount(
			LPCTSTR, 
			std::basic_string<TCHAR> &, 
			std::basic_string<TCHAR> &) const;

		bool IsValid() const;
		bool IsValid(LPCTSTR) const;
		bool GetSidUse(LPCTSTR, SID_NAME_USE &) const;    

		bool SetString(LPCTSTR pszSid);
		bool GetString(std::basic_string<TCHAR> &) const;

		std::basic_string<TCHAR> Dump() const;
		std::basic_string<TCHAR> DumpXML() const;

	private:
		void Free();	
		void CreateSIDFromList(const SID_IDENTIFIER_AUTHORITY &auth, 
			BYTE nSubAuthorityCount, va_list marker);	
		
		PSID m_pSid;
	};
}

/*
****************************************************************************
* Generate NULL sid
****************************************************************************
*/
#define NULLSID(theSid)													\
    theSid.CreateNullSID(1, SECURITY_NULL_RID)

/*
****************************************************************************
* Generate CREATOR OWNER sid
****************************************************************************
*/
#define CREATOROWNERSID(theSid)											\
    theSid.CreateCreatorSID(1, SECURITY_CREATOR_OWNER_RID)

/*
****************************************************************************
* Generate CREATOR GROUP sid
****************************************************************************
*/
#define CREATORGROUPSID(theSid)											\
    theSid.CreateCreatorSID(1, SECURITY_CREATOR_GROUP_RID)

/*
****************************************************************************
* Generate LOCAL sid
****************************************************************************
*/
#define LOCALSID(theSid)										        \
    theSid.CreateLocalSID(1, SECURITY_LOCAL_RID)

/*
****************************************************************************
* Generate WORLD sid (everyone group)
****************************************************************************
*/
#define WORLDSID(theSid)										        \
    theSid.CreateWorldSID(1, SECURITY_WORLD_RID)

/*
****************************************************************************
* Generate LOCAL ADMINSTRATORS GROUP sid 
* A local group used for administration of the domain.
****************************************************************************
*/
#define LOCALADMINSSID(theSid)											\
    theSid.CreateNTSID(2, SECURITY_BUILTIN_DOMAIN_RID,                  \
        DOMAIN_ALIAS_RID_ADMINS)

/*
****************************************************************************
* Generate LOCAL GUESTS GROUP sid
* A local group representing guests of the domain.
****************************************************************************
*/
#define LOCALGUESTSSID(theSid)											\
    theSid.CreateNTSID(2, SECURITY_BUILTIN_DOMAIN_RID,                  \
        DOMAIN_ALIAS_RID_GUESTS)

/*
****************************************************************************
* Generate LOCAL USERS GROUP sid
* A local group representing all users in the domain.
****************************************************************************
*/
#define LOCALUSERSSID(theSid)											\
    theSid.CreateNTSID(2, SECURITY_BUILTIN_DOMAIN_RID,                  \
        DOMAIN_ALIAS_RID_USERS)

/*
****************************************************************************
* Generate LOCAL POWER USERS GROUP sid
* A local group used to represent a user or set of users who expect to treat 
* a system as if it were their personal computer rather than as a 
* workstation for multiple users.
****************************************************************************
*/
#define LOCALPOWERUSERSSID(theSid)										\
    theSid.CreateNTSID(2, SECURITY_BUILTIN_DOMAIN_RID,                  \
        DOMAIN_ALIAS_RID_POWER_USERS)

/*
****************************************************************************
* Generate LOCAL ACCOUNT OPERATIONS GROUP sid
* A local group existing only on systems running Windows NT Server/Windows 
* 2000 Server. This local group permits control over non-administrator 
* accounts.
****************************************************************************
*/
#define LOCALACCOUNTSOPSSID(theSid)										\
    theSid.CreateNTSID(2, SECURITY_BUILTIN_DOMAIN_RID,                  \
        DOMAIN_ALIAS_RID_ACCOUNT_OPS)

/*
****************************************************************************
* Generate LOCAL SYSTEM OPERATIONS GROUP sid
* A local group existing only on systems running Windows NT Server/Windows 
* 2000 Server. This local group performs system administrative functions, 
* not including security functions. It establishes network shares, controls 
* printers, unlocks workstations, and performs other operations.
****************************************************************************
*/
#define LOCALSYSTEMOPSSID(theSid)										\
    theSid.CreateNTSID(2, SECURITY_BUILTIN_DOMAIN_RID,                  \
        DOMAIN_ALIAS_RID_SYSTEM_OPS)

/*
****************************************************************************
* Generate LOCAL PRINTERS GROUP sid
* A local group existing only on systems running Windows NT Server/Windows 
* 2000 Server. This local group controls printers and print queues.
****************************************************************************
*/
#define LOCALPRINTEROPSSID(theSid)										\
    theSid.CreateNTSID(2, SECURITY_BUILTIN_DOMAIN_RID,                  \
        DOMAIN_ALIAS_RID_PRINT_OPS)

/*
****************************************************************************
* Generate LOCAL BACKUP OPERATIONS GROUP sid
* A local group used for controlling assignment of file backup-and-restore 
* privileges.
****************************************************************************
*/
#define LOCALBACKUPOPSSID(theSid)										\
    theSid.CreateNTSID(2, SECURITY_BUILTIN_DOMAIN_RID,                  \
        DOMAIN_ALIAS_RID_BACKUP_OPS)

/*
****************************************************************************
* Generate LOCAL REPLICATORS GROUP sid
* A local group responsible for copying security databases from the primary 
* domain controller to the backup domain controllers. These accounts are 
* used only by the system.
****************************************************************************
*/
#define LOCALREPLICATORSSID(theSid)										\
    theSid.CreateNTSID(2, SECURITY_BUILTIN_DOMAIN_RID,                  \
        DOMAIN_ALIAS_RID_REPLICATOR)

