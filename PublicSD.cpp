#include <assert.h>
#include "PublicSD.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CPublicSD members.
//////////////////////////////////////////////////////////////////////////////
CPublicSD::CPublicSD()
{
	m_pSD = NULL;
	m_pACL = NULL;
	m_pAdminSID = NULL;
	m_pEveryoneSID = NULL;

	bool bResult = false;
	SID_IDENTIFIER_AUTHORITY SIDAuthNT = SECURITY_NT_AUTHORITY;
	SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;

	// Create a well-known SID for the Everyone group.
	if (::AllocateAndInitializeSid(&SIDAuthWorld, 1, SECURITY_WORLD_RID,
		0, 0, 0, 0, 0, 0, 0, &m_pEveryoneSID))
	{
		// Initialize an EXPLICIT_ACCESS structure for an ACE.  The ACE will allow 
		// Everyone read access to the key.
		EXPLICIT_ACCESS ea[2];

		ZeroMemory(&ea, 2 * sizeof(EXPLICIT_ACCESS));
		ea[0].grfAccessPermissions = KEY_READ;
		ea[0].grfAccessMode = SET_ACCESS;
		ea[0].grfInheritance = NO_INHERITANCE;
		ea[0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
		ea[0].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
		ea[0].Trustee.ptstrName = (LPTSTR)m_pEveryoneSID;

		// Create a SID for the BUILTIN\Administrators group.
		if (::AllocateAndInitializeSid(&SIDAuthNT, 2,
			SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS,
			0, 0, 0, 0, 0, 0, &m_pAdminSID))
		{
			// Initialize an EXPLICIT_ACCESS structure for an ACE.  The ACE will 
			// allow the Administrators group full access to the key.
			ea[1].grfAccessPermissions = KEY_ALL_ACCESS;
			ea[1].grfAccessMode = SET_ACCESS;
			ea[1].grfInheritance = NO_INHERITANCE;
			ea[1].Trustee.TrusteeForm = TRUSTEE_IS_SID;
			ea[1].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
			ea[1].Trustee.ptstrName = (LPTSTR)m_pAdminSID;

			if (::SetEntriesInAcl(2, ea, NULL, &m_pACL) == ERROR_SUCCESS)
			{
				// Initialize a security descriptor.   
				m_pSD = (PSECURITY_DESCRIPTOR)
					::LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH);

				if (m_pSD != NULL)
				{
					if (::InitializeSecurityDescriptor(m_pSD,
						SECURITY_DESCRIPTOR_REVISION))
					{
						// Add the ACL to the security descriptor.              
						if (::SetSecurityDescriptorDacl(m_pSD, TRUE, m_pACL, FALSE))
							bResult = true;
					}
				}
			}
		}
	}

	if (!bResult)
		Clear();
}

CPublicSD::~CPublicSD()
{
	Clear();
}

PSECURITY_DESCRIPTOR CPublicSD::GetSD() const
{
	return m_pSD;
}

void CPublicSD::Clear()
{
	if (m_pEveryoneSID != NULL)
	{
		::FreeSid(m_pEveryoneSID);
		m_pEveryoneSID = NULL;
	}

	if (m_pAdminSID != NULL)
	{
		::FreeSid(m_pAdminSID);
		m_pAdminSID = NULL;
	}

	if (m_pACL != NULL)
	{
		::LocalFree(m_pACL);
		m_pACL = NULL;
	}

	if (m_pSD != NULL)
	{
		::LocalFree(m_pSD);
		m_pSD = NULL;
	}
}
