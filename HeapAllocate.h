#pragma once

#include <windows.h>

namespace NTSecurity
{
	class CHeapAllocate
	{
	public:
		CHeapAllocate();
		CHeapAllocate(size_t);
		~CHeapAllocate();

		LPVOID Alloc(size_t);
		LPVOID Detach();
		
		void Free();	
		template <typename Ptr> static void Free(Ptr &);

	private:
		LPVOID m_pMem;
	};

	//////////////////////////////////////////////////////////////////////////
	// CHeapAllocate members.
	//////////////////////////////////////////////////////////////////////////
	template <typename Ptr>
	void CHeapAllocate::Free(Ptr & pMem)
	{
		if (pMem != nullptr)
		{
			::HeapFree(::GetProcessHeap(), 0, pMem);
			pMem = nullptr;
		}
	}
}
