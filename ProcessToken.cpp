#include <assert.h>
#include "ProcessToken.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CProcessToken members.
//////////////////////////////////////////////////////////////////////////////
CProcessToken::CProcessToken(DWORD dwAccess, HANDLE hProcess) : m_hToken(NULL)
{
	if (!::OpenProcessToken(hProcess, dwAccess, &m_hToken))
		m_hToken = NULL;
}

CProcessToken::~CProcessToken()
{
	if (m_hToken) 
		::CloseHandle(m_hToken);
}

CProcessToken::operator HANDLE()
{
	return GetHANDLE();
}

HANDLE CProcessToken::GetHANDLE()
{
	return m_hToken;
}
