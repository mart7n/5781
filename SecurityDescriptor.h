#pragma once

#include <windows.h>
#include <tchar.h>
#include <string>
#include <aclapi.h>
#include "SID.h"
#include "ACL.h"

namespace NTSecurity
{
	/**
	****************************************************************************
	* Security descriptors - a security descriptor is used to describe the
	* security associated with an NT resource.  A security descriptor contains
	* a security identifier for the owner of the resource (owner SID), a 
	* security identifier for the primary group (group SID), a discretionary 
	* access control list (DACL) and a system access control list (SACL).
	****************************************************************************
	*/
	class CSecurityDescriptor
	{
	public:
		CSecurityDescriptor(const PSECURITY_DESCRIPTOR = NULL);
		CSecurityDescriptor(const CSecurityDescriptor &);
		~CSecurityDescriptor();

		PSECURITY_DESCRIPTOR operator=(const PSECURITY_DESCRIPTOR);
		CSecurityDescriptor & operator=(const CSecurityDescriptor &);

		operator PSECURITY_DESCRIPTOR();
		operator const PSECURITY_DESCRIPTOR() const;

		bool SetDACLDefault();	
		bool SetDACL(const PACL);	

		PACL GetDACL() const;
		bool IsDefaultDACL() const;
		bool IsPresentDACL() const;

		bool SetSACLDefault();	
		bool SetSACL(const PACL);

		PACL GetSACL() const;
		bool IsDefaultSACL() const;
		bool IsPresentSACL() const;

		bool SetSIDOwnerDefault();
		bool SetSIDOwner(const PSID);

		bool IsDefaultSIDOwner() const;
		PSID GetSIDOwner() const;

		bool SetSIDGroupDefault();
		bool SetSIDGroup(const PSID);

		bool IsDefaultSIDGroup() const;
		PSID GetSIDGroup() const;

		enum Info 
		{ 
			UpdateDacl	= DACL_SECURITY_INFORMATION,
			UpdateSacl	= SACL_SECURITY_INFORMATION,
			UpdateGroup	= GROUP_SECURITY_INFORMATION,
			UpdateOwner	= OWNER_SECURITY_INFORMATION 
		};

		/*
		*************************************************************************
		* Set security information for file
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool GetFromFile(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner);
		
		bool GetFromFile(
			LPCTSTR, 
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner);

		/*
		*************************************************************************
		* obtain security information for a service (local or remote)
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool GetFromService(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner);
		
		/*
		*************************************************************************
		* obtain security information for a printer (locate or remote)
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool GetFromPrinter(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner);
		
		/*
		*************************************************************************
		* obtain security information for registry
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool GetFromRegistryKey(
			HKEY, 
			LPCTSTR, 
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner);

		/*
		*************************************************************************
		* obtain security information for a registry key
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool GetFromRegistryKey(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner);

		/*
		*************************************************************************
		* obtain security information for a network share (local or remote)
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool GetFromNetworkShare(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner);

		/*
		*************************************************************************
		* obtain security information for a local kernel object, which includes 
		* the following object types: process, thread, job, semaphore, event, 
		* mutex, file mapping, waitable timer, access token, named pipe, or 
		* anonymous pipe. 
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool GetFromKernelObject(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner);

		/*
		*************************************************************************
		* obtain security information for a window station or desktop object on 
		* the local computer
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool GetFromWindowObject(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner);

		/*
		*************************************************************************
		* update security information for a file
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool SetToFile(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner) const;

		bool SetToFile(
			LPCTSTR, 
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner) const;

		/*
		*************************************************************************
		* update security information for a service (local or remote)
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool SetToService(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner) const;

		/*
		*************************************************************************
		* update security information for a printer (locate or remote)
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool SetToPrinter(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner) const;
		
		/*
		*************************************************************************
		* update security information for a registry key
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool SetToRegistryKey(
			HKEY, 
			LPCTSTR, 
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner);

		bool SetToRegistyKey(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner) const;

		/*
		*************************************************************************
		* update security information for a network share (local or remote)
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool SetToNetworkShare(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup |UpdateOwner) const;
		
		/*
		*************************************************************************
		* update security information for a local kernel object, which includes 
		* the following object types: process, thread, job, semaphore, event, 
		* mutex, file mapping, waitable timer, access token, named pipe, or 
		* anonymous pipe. 
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool SetToKernelObject(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner) const;

		/*
		*************************************************************************
		* update security information for a window station or desktop object on 
		* the local computer
		* If false is returned then GetLastError() can be used to obtain details.
		*************************************************************************
		*/
		bool SetToWindowObject(
			HANDLE,
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner) const;

		std::basic_string<TCHAR> Dump() const;
		std::basic_string<TCHAR> DumpXML() const;

		bool GetFromObject(
			HANDLE, 
			SE_OBJECT_TYPE, 
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner);

		bool SetToObject(
			HANDLE, 
			SE_OBJECT_TYPE, 
			DWORD = UpdateDacl | UpdateSacl | UpdateGroup | UpdateOwner) const;

	private:
		void Free();
		void CreateEmpty();
		void EnsureAvailable();

		PSECURITY_DESCRIPTOR	m_pSD;
		CACL					m_dacl;
		CACL					m_sacl;
		CSID					m_sidOwner;
		CSID					m_sidGroup;		
	};
}
