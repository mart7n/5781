#pragma once

#include <windows.h>
#include <aclapi.h>

namespace NTSecurity
{
	class SIDs
	{
	public:
		// WARNING: raw pointers returned that need managing.
		static DWORD GetLogOnAccountSID(SID * &);
		static DWORD GetCurrentProcessUserTokens(PTOKEN_USER &);
	};
}
