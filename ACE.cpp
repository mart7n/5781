#include <assert.h>
#include "ACE.h"
#include "SID.h"
#include "HeapAllocate.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CACE members.
//////////////////////////////////////////////////////////////////////////////
CACE::CACE(const PACE pAce) : m_pAce(NULL)
{
	*this = pAce;
}

CACE::CACE(const CACE &other) : m_pAce(NULL) 
{ 
	*this = other.m_pAce; 
}

CACE::~CACE() 
{ 
	Free(); 
}

void CACE::Free()
{
	CHeapAllocate::Free(m_pAce);	
}

CACE & CACE::operator=(const CACE &other) 
{ 
	*this = other.m_pAce; 
	return *this;
}

CACE::operator PACE() 
{ 
	return m_pAce; 
}

CACE::operator const PACE() const 
{ 
	return m_pAce; 
}	

BYTE CACE::GetType() const 
{ 
	return m_pAce ? ((ACE_HEADER*)m_pAce)->AceType : 0; 
}

ACCESS_MASK CACE::GetAccessMask() const 
{ 
	return m_pAce ? ((ACCESS_ALLOWED_ACE*)m_pAce)->Mask : 0; 
}

BYTE CACE::GetControlFlags() const 
{ 
	return m_pAce ? ((ACE_HEADER*)m_pAce)->AceFlags : 0; 
}

DWORD CACE::GetSize() const 
{ 
	return m_pAce ? ((ACE_HEADER*)m_pAce)->AceSize : 0; 
}

PACE CACE::operator=(const PACE pAce)
{
	if ((LPBYTE)pAce != (LPBYTE)m_pAce)
	{
		Free();

		if (pAce)
		{			
			PSID pSid = &((ACCESS_ALLOWED_ACE*)pAce)->SidStart;
			DWORD cbSid = ::GetLengthSid(pSid);
			DWORD cbAce = sizeof(ACCESS_ALLOWED_ACE) - sizeof(DWORD) + cbSid;

			CHeapAllocate allocator(cbAce);
			m_pAce = (PSID)allocator.Detach();

			// Shallow copy first part of ace			
			*((ACCESS_ALLOWED_ACE *)m_pAce) = *((ACCESS_ALLOWED_ACE *)pAce);

			::CopySid(cbSid, &(((ACCESS_ALLOWED_ACE*)m_pAce)->SidStart), 
				(PSID)pSid);

			//////////////////////////////////////////////////////////////////////////////			
			//		switch(((ACE_HEADER *)pAce)->AceType)
			//		{
			//				
			//		case ACCESS_ALLOWED_ACE_TYPE:
			//			m_pAce = new ACCESS_ALLOWED_ACE;
			//			*((ACCESS_ALLOWED_ACE *)m_pAce) = *((ACCESS_ALLOWED_ACE *)pAce);
			//			break;
			//				
			//		case ACCESS_DENIED_ACE_TYPE:
			//			m_pAce = new ACCESS_DENIED_ACE;
			//			*((ACCESS_DENIED_ACE *)m_pAce) = *((ACCESS_DENIED_ACE *)pAce);
			//			break;
			//				
			//		case SYSTEM_AUDIT_ACE_TYPE:
			//			m_pAce = new SYSTEM_AUDIT_ACE;
			//			*((SYSTEM_AUDIT_ACE *)m_pAce) = *((SYSTEM_AUDIT_ACE *)pAce);
			//				
			//		}
			//////////////////////////////////////////////////////////////////////////////
		}
	}

	return m_pAce;
}

bool CACE::IsAllowedAccess() const
{ 
	if (!m_pAce)
		return false;

	return ((ACE_HEADER*)m_pAce)->AceType == ACCESS_ALLOWED_ACE_TYPE; 
}

bool CACE::IsDeniedAccess() const
{
	if (!m_pAce)
		return false;

	return ((ACE_HEADER*)m_pAce)->AceType == ACCESS_DENIED_ACE_TYPE; 
}

bool CACE::IsSystemAudit() const 
{
	if (!m_pAce)
		return false;

	return ((ACE_HEADER*)m_pAce)->AceType == SYSTEM_AUDIT_ACE_TYPE; 
}
 
PSID CACE::GetSID() const	
{
	if (!m_pAce)
		return NULL;

	return (PSID)&((PACCESS_ALLOWED_ACE)m_pAce)->SidStart;
}

void CACE::SetSID(const PSID pSid)
{
	CSID defaultSid;
	PSID pUseSid = pSid;

	if (!pSid)
	{
		defaultSid.CreateNullSID();
		pUseSid = (PSID)defaultSid;
	}

	DWORD cbSid = ::GetLengthSid(pUseSid);
	DWORD cbAce = sizeof(ACCESS_ALLOWED_ACE) - sizeof(DWORD) + cbSid;

	CHeapAllocate allocator(cbAce);
	PACE pAce = (PSID)allocator.Detach();

	if (m_pAce)
	{
		// Shallow copy part of previous ace		
		*((ACCESS_ALLOWED_ACE*)pAce) = *((ACCESS_ALLOWED_ACE*)m_pAce);
	}
	// Set defaults for ace if no previous ace exists	
	else 
	{
		// Default ace is access allowed		
		((ACE_HEADER*)pAce)->AceType = ACCESS_ALLOWED_ACE_TYPE;
		((ACE_HEADER*)pAce)->AceFlags = 0; //CONTAINER_INHERIT_ACE;		

		((ACCESS_ALLOWED_ACE*)pAce)->Mask = FILE_ALL_ACCESS;
	}

	((ACE_HEADER*)pAce)->AceSize = (WORD)cbAce;
	::CopySid(cbSid, &((ACCESS_ALLOWED_ACE*)pAce)->SidStart, pUseSid);

	Free();
	m_pAce = pAce;
}

void CACE::SetType(BYTE nType)
{
	if (!m_pAce)
		SetSID(NULL);

	((ACE_HEADER*)m_pAce)->AceType = nType;
}

void CACE::SetControlFlags(BYTE nFlags)
{
	if (!m_pAce)
		SetSID(NULL);

	((ACE_HEADER*)m_pAce)->AceFlags = nFlags;
}

void CACE::SetAccessMask(ACCESS_MASK mask)
{
	if (!m_pAce)
		SetSID(NULL);

	((ACCESS_ALLOWED_ACE*)m_pAce)->Mask = mask;
}
 
void CACE::SetAllowedAccess()
{
	if (!m_pAce)
		SetSID(NULL);

	((ACE_HEADER*)m_pAce)->AceType = ACCESS_ALLOWED_ACE_TYPE; 
}

void CACE::SetDeniedAccess()
{
	if (!m_pAce)
		SetSID(NULL);

	((ACE_HEADER*)m_pAce)->AceType = ACCESS_DENIED_ACE_TYPE; 
}

void CACE::SetSystemAudit()
{
	if (!m_pAce)
		SetSID(NULL);

	((ACE_HEADER*)m_pAce)->AceType = SYSTEM_AUDIT_ACE_TYPE; 
}

DWORD CACE::GetSizeACE(const PACE lpAce)
{
	if (!lpAce)
		return 0;

	// Default size of sid as 4 (size of DWORD)
	DWORD cbSid = 4;
	PSID pSid = &((ACCESS_ALLOWED_ACE*)lpAce)->SidStart;
	
	if (::IsValidSid(pSid)) //*((DWORD*)pSid))
		cbSid = ::GetLengthSid(pSid);
	
	switch(((ACE_HEADER *)lpAce)->AceType)
	{
	case ACCESS_ALLOWED_ACE_TYPE:
		return sizeof(ACCESS_ALLOWED_ACE) - sizeof(DWORD) + cbSid;		

	case ACCESS_DENIED_ACE_TYPE:
		return sizeof(ACCESS_DENIED_ACE) - sizeof(DWORD) + cbSid;		

	case SYSTEM_AUDIT_ACE_TYPE:
		return sizeof(SYSTEM_AUDIT_ACE) - sizeof(DWORD) + cbSid;		

	default:
		return 0;		
	}
}

 /**
 ****************************************************************************
 * Validates an ACE.  Checks whether the sid is valid, the type specified
 * in the ace header is valid, and that the size of the ace specifed in the
 * ace header matches the actual size of the ace.
 ****************************************************************************
 */
bool CACE::IsValid(LPCTSTR lpszSystem) const
{
	if (!m_pAce)
		return false;

	CSID sid(&((ACCESS_ALLOWED_ACE*)m_pAce)->SidStart);
	if (!sid.IsValid(lpszSystem))
	{
		return false;
	}

	switch(((ACE_HEADER *)m_pAce)->AceType)
	{
	case ACCESS_ALLOWED_ACE_TYPE:		
	case ACCESS_DENIED_ACE_TYPE:		
	case SYSTEM_AUDIT_ACE_TYPE:
		break;

	default:
		return false;		
	}

	// Ensure the size specified in the header equates to the size for the 
	// specific type of ACE plus the size of the contained sid
	if (GetSizeACE(m_pAce) != ((ACE_HEADER*)m_pAce)->AceSize)
		return false;

	return true;
}

std::basic_string<TCHAR> CACE::Dump() const
{
	if (!m_pAce)
		return std::basic_string<TCHAR>(_T("ACE - Unallocated"));

	std::basic_string<TCHAR> strResults(_T("ACE - type: "));

	switch(((ACE_HEADER *)m_pAce)->AceType)
	{
	default: strResults += _T("Unsupported"); break;
	case SYSTEM_AUDIT_ACE_TYPE: strResults += _T("SYSTEM_AUDIT_ACE"); break;
	case ACCESS_DENIED_ACE_TYPE: strResults += _T("ACCESS_DENIED_ACE");	break;
	case ACCESS_ALLOWED_ACE_TYPE: strResults += _T("ACCESS_ALLOWED_ACE"); break;
	}

	TCHAR szSize[10];

	::_ltot_s(GetSize(), szSize, 10);
	strResults += _T(", Size: ");
	strResults += szSize;

	::_ltot_s(GetAccessMask(), szSize, 16);
	strResults += _T(", Access Mask: 0x");
	strResults += szSize;

	::_ltot_s(GetControlFlags(), szSize, 10);
	strResults += _T(", Control Flags: ");
	strResults += szSize;

	strResults += _T(", ");
	strResults += CSID(GetSID()).Dump();

	return strResults;
}

std::basic_string<TCHAR> CACE::DumpXML() const
{
	if (!m_pAce)
		return std::basic_string<TCHAR>(_T("<ACE/>"));

	std::basic_string<TCHAR> strResults(_T("<ACE Type="));

	switch(((ACE_HEADER *)m_pAce)->AceType)
	{
	default: strResults += _T("Unsupported"); break;
	case SYSTEM_AUDIT_ACE_TYPE: strResults += _T("SYSTEM_AUDIT_ACE"); break;
	case ACCESS_DENIED_ACE_TYPE: strResults += _T("ACCESS_DENIED_ACE");	break;
	case ACCESS_ALLOWED_ACE_TYPE: strResults += _T("ACCESS_ALLOWED_ACE"); break;
	}

	TCHAR szSize[10];

	::_ltot_s(GetSize(), szSize, 10);
	strResults += _T(" Size=");
	strResults += szSize;

	::_ltot_s(GetAccessMask(), szSize, 16);
	strResults += _T(", AccessMask=0x");
	strResults += szSize;

	::_ltot_s(GetControlFlags(), szSize, 10);
	strResults += _T(", ControlFlags=");
	strResults += szSize;

	strResults += _T(">");
	strResults += CSID(GetSID()).DumpXML();

	strResults += _T("</ACE>");
	return strResults;
}
