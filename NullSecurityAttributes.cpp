#include <assert.h>
#include "NullSecurityAttributes.h"

using namespace NTSecurity;

//////////////////////////////////////////////////////////////////////////////
// CNullSecurityAttributes members.
//////////////////////////////////////////////////////////////////////////////
CNullSecurityAttributes::CNullSecurityAttributes()
{
	::InitializeSecurityDescriptor(&m_sd, SECURITY_DESCRIPTOR_REVISION);
	m_sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	m_sa.lpSecurityDescriptor = &m_sd;
	m_sa.bInheritHandle = FALSE;
}

SECURITY_ATTRIBUTES * CNullSecurityAttributes::operator &()
{
	return &m_sa;
}

const SECURITY_ATTRIBUTES * CNullSecurityAttributes::operator &() const
{
	return &m_sa;
}
